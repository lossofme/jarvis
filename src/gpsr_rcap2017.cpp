#include <ros/ros.h>
#include <math.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Pose.h>
#include "sensor_msgs/JointState.h"
#include <geometry_msgs/Transform.h>
#include "std_msgs/Float32.h"
#include <std_msgs/Float64.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/Int64.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Char.h>
#include <xy_to_xyz_msgs/BoundingBoxes_XYZ.h>
#include <xy_to_xyz_msgs/BoundingBox_XYZ.h>
#include <speech_jv/target.h>
#include <string>
#include <iostream>
#include <sound_play/sound_play.h>
#include <std_msgs/Bool.h>

#define JOY_BUTTON_X 0
#define JOY_BUTTON_Y 3
#define JOY_BUTTON_A 1
#define JOY_BUTTON_B 2
#define JOY_BUTTON_L1 4
#define JOY_BUTTON_R1 5
#define JOY_BUTTON_R2 7
#define JOY_AXES_UPDOWN1 7
#define JOY_AXES_UPDOWN2 4
#define JOY_AXES_SLIP 6
#define JOY_AXES_LEFTRIGHT 0
#define JOY_AXES_TILTHEAD_X 2
#define JOY_AXES_TILTHEAD_Y 3
#define PI 3.1415926535897932384626433
#define ACCEPT_ERROR 0.01
#define ACCEPT_ERROR2 0.04

ros::Publisher chatter_pub,mani_pub1,mani_pub2,mani_pub3,mani_pub4,mani_pub5,head0_pub,head1_pub,navi_pub,simhead0_pub,simhead1_pub;
ros::Subscriber feedback_arm0,feedback_arm1,feedback_arm2,feedback_arm3,feedback_arm4,feedback_head0,feedback_head1,sub,sub2,sub3,door_sub,sub_speech_,hark_sub;

geometry_msgs::Transform point;
geometry_msgs::Pose nav_pose;
sensor_msgs::JointState joint;

std_msgs::Float64 se1;
std_msgs::Float64 se2;
std_msgs::Float64 se3;
std_msgs::Float64 se4;
std_msgs::Float64 se5;
std_msgs::Float64 head_joint_pos0;
std_msgs::Float64 head_joint_pos1;
speech_jv::target text;
std_msgs::Bool door;
double arm0,arm1,arm2,arm3,arm4,head0,head1;
double px,py,pz,sea,seo,sen,nx,ny,nz,ox,oy,oz,ax,ay,az,a2,a3;
int JOY_BUTTON[11];
float JOY_AXES[8];
int state = 1;
int check = 2;
int m_state = 0;
int m_check = 0;
int check_inverse = 0;
int finish_navi = 0;
int state_navi = 0;
int check_navi = 0;
int state_hark = 0;
xy_to_xyz_msgs::BoundingBoxes_XYZ img;

bool sound_true=0;
bool object_true=0;


int count = 0;
double csc(double x)
{
  return 1/sin(x);
}
double cot(double x)
{
  return 1/tan(x);
}
void NavCallback(const std_msgs::Int64& msg)
{
  finish_navi = msg.data;
}
void arm0Callback(const dynamixel_msgs::JointState& a0)
{
  arm0 = abs(a0.error);
}
void arm1Callback(const dynamixel_msgs::JointState& a1)
{
  arm1 = abs(a1.error);
}
void arm2Callback(const dynamixel_msgs::JointState& a2)
{
  arm2 = abs(a2.error);
}
void arm3Callback(const dynamixel_msgs::JointState& a3)
{
  arm3 = abs(a3.error);
}
void arm4Callback(const dynamixel_msgs::JointState& a4)
{
  arm4 = abs(a4.error);
}
void head0Callback(const dynamixel_msgs::JointState& h0)
{
  head0 = abs(h0.error);
}
void head1Callback(const dynamixel_msgs::JointState& h1)
{
  head1 = abs(h1.error);
}
void ImageCallback(const xy_to_xyz_msgs::BoundingBoxes_XYZ::ConstPtr& set)
{
  try
  {

    img=*set;

    if (check_inverse != 1)
      for(uint8_t i=0; i< img.boundingBoxes_xyz.size(); i++)
      {
       if(img.boundingBoxes_xyz[i].Class == "bottle" && img.boundingBoxes_xyz[i].probability > 0.55 )
        {
          point.translation.x = img.boundingBoxes_xyz[i].x;
          point.translation.y = img.boundingBoxes_xyz[i].y;
          point.translation.z = img.boundingBoxes_xyz[i].z;
          ROS_INFO("Found it");
          object_true=1;
           //break;
        }
      }
      ROS_INFO("asd");
  }
  catch(int x)
  {
    ROS_INFO("CB ERROR");
    // continue;
  }
}

int check_pos_arm()
{
  if(arm0 < ACCEPT_ERROR && arm1 < ACCEPT_ERROR && arm2 < ACCEPT_ERROR && arm3 < ACCEPT_ERROR)
    return 1;
  else
    return 0;
}
int check_pos_head()
{
  if(head0 < ACCEPT_ERROR && head1 < ACCEPT_ERROR)
    return 1;
  else
    return 0;
}
int check_pos_gripper()
{
  if(arm4 < ACCEPT_ERROR2)
    return 1;
  else
    return 0;
}
void JoyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
	for (int i=0; i<11 ;i++)
		JOY_BUTTON[i]=msg->buttons[i];
	for (int j=0; j<8 ;j++)
		JOY_AXES[j]=msg->axes[j];
}
void inverse_kinematic(geometry_msgs::Transform pos,std_msgs::Float64 & s1,std_msgs::Float64 & s2,std_msgs::Float64 & s3,std_msgs::Float64 & s4)
{

  ROS_INFO("inverse");
  // float a=0.04;
  // float b=0.17;
  // float c=0.3;
  
  float a=-0.04;
  float b=-0.3;
  float c=-0.17;
  float d=pos.translation.z;
  float e=pos.translation.y;
  float f=-pos.translation.x;
  float s=-PI/2;
  float bb=0.8;
  // px = a+ e*cos(b)+g*cos(s)*sin(b)-f*sin(s)  
  // py = b+f*cos(s)+e*cos(b)*sin(s)+g*sin(b)*sin(s)
  // pz = c+g*cos(b)-e*sin(s)
  // sea = s;
  // seo = b;
  // sen = PI/2;

  // px = pos.translation.x;  
  // py = -0.3;
  // pz = 0.3;
  // // sea = s;
  // seo = b;
  // sen = PI/2;

if(pos.translation.x > 0.1)
{
  px = -pos.translation.x-0.05;   
  py = -pos.translation.z*0.45;
  pz = pos.translation.y*0.85;
}
else if(pos.translation.x < 0.1)
{
  px = -pos.translation.x-0.05;   
  py = -pos.translation.z*0.45;
  pz = pos.translation.y*0.75;
}
else
{
  px = -pos.translation.x-0.05;   
  py = -pos.translation.z*0.3;
  pz = pos.translation.y*0.85;
}



  // px = pos.translation.x;   
  // py = pos.translation.y+0.2;
  // pz = pos.translation.z-0.3;
  sea = 0;
  seo = PI/2;
  sen = 0;
  



  // sea = pos.rotation.x;
  // seo = pos.rotation.y;
  // sen = pos.rotation.z;

  a2 = 0.175;
  a3 = 0.1702;
  nx = cos(sea)*cos(seo);
  ny = cos(seo)*sin(sea);
  nz = -sin(seo);
  ox = (-cos(sen)*sin(sea))+(cos(sea)*sin(sen)*sin(seo));
  oy = (cos(sea)*cos(sen))+(sin(sea)*sin(sen)*sin(seo));
  oz = cos(seo)*sin(sen);
  ax = (sin(sea)*sin(sen))+(cos(sea)*cos(sen)*sin(seo));
  ay = (-cos(sea)*sin(sen))+(cos(sen)*sin(sea)*sin(seo));
  az = cos(sen)*cos(seo);

  s1.data = atan2((py/(sqrt(pow(px,2)+pow(py,2)))),(px/(sqrt(pow(px,2)+pow(py,2)))));
  s3.data = -acos((-pow(a2,2)-pow(a3,2)+pow(px,2)+pow(py,2)+pow(pz,2))/(2*a2*a3));
  s2.data = atan2(((-cos(s1.data)*a3*px)+(-sin(s1.data)*a3*py)+(csc(s3.data)*a2*pz)+(cot(s3.data)*a3*pz)),((cos(s1.data)*csc(s3.data)*a2*px)+(cos(s1.data)*cot(s3.data)*a3*px)+(csc(s3.data)*sin(s1.data)*a2*py)+(cot(s3.data)*sin(s1.data)*a3*py)+(a3*pz)))+(PI/2);
  s4.data = asin(nz)-s2.data-s3.data;
    
  //printf("%.2f  %.2f  %.2f  %.2f  %.2f  %.2f\n",se1,se2,se3,se4,se5,se6 );
  //px = px +0.001;
  if(((px*px)+(py*py)+(pz*pz))>((a2+a3+0.2)*(a2+a3+0.2))||(((px*px)+(py*py)+(pz*pz)))<((a2-a3)*(a2-a3)))
  {
    printf("Out of Range\n");
    printf("%.2lf  %.2lf  %.2lf\n",px,py,pz);
    check_inverse = 10;
  }
  else
  {
  

  joint.header.stamp = ros::Time::now();
    joint.name.resize(4);
    joint.position.resize(4);
    joint.name.push_back("right_upper_arm_joint");
    joint.name.push_back("right_elbow_joint");
    joint.name.push_back("right_wrist_joint");
    joint.name.push_back("right_wrist_joint2");
    

    joint.position.push_back(s1.data);
    joint.position.push_back(s2.data);
    joint.position.push_back(s3.data);
    joint.position.push_back(s4.data);


    printf("%.2lf  %.2lf  %.2lf\n",px,py,pz);
    chatter_pub.publish(joint);
    s1.data = -1*s1.data;
    s2.data = (-1*s2.data)+0.1;
    s3.data = s3.data-0.1;
    s4.data = -1*s4.data;
    check_inverse = 1;
  }
}
void head_scan(std_msgs::Float64 & x,std_msgs::Float64 & y)
{
  x.data = -1.57;
  y.data = 0.5;
  head0_pub.publish(x);
  head1_pub.publish(y);
  // if(check_pos_head() ==1)
  // {
  //   simhead0_pub.publish(x);
  //   simhead1_pub.publish(y); 
  // } 
}
void head_localization(double x)
{

  head0_pub.publish(x);

  // if(check_pos_head() ==1)
  // {
  //   simhead0_pub.publish(x);
  //   simhead1_pub.publish(y); 
  // } 
}
void prepare_arm(std_msgs::Float64 & s1,std_msgs::Float64 & s2,std_msgs::Float64 & s3,std_msgs::Float64 & s4)
{
  s1.data = 0.0;
  s2.data = -1.2;
  s3.data = -1.6;
  s4.data = 1.0;
  mani_pub1.publish(0.0);
  mani_pub3.publish(-1.6);
  mani_pub2.publish(-1.2);
  mani_pub4.publish(1.0);
}
void prepare_head(std_msgs::Float64 & x,std_msgs::Float64 & y)
{
  x.data = 0;
  y.data = 0;
  head0_pub.publish(x);
  head1_pub.publish(y);
  // if(check_pos_head() ==1)
  // {
  //   simhead0_pub.publish(x);
  //   simhead1_pub.publish(y); 
  // } 
}
void manipulation(std_msgs::Float64 & s1,std_msgs::Float64 & s2,std_msgs::Float64 & s3,std_msgs::Float64 & s4)
{
  state = 1;
  check = 2;
  while(state < 6)
  {
    printf("mani : %d  %d\n",state,check );
    switch(state)
    {
      case 1 : {
                  if(check == 2){
                        mani_pub3.publish(-2.07);
                        mani_pub2.publish(0.7);
                        //mani_pub1.publish(1.0);
                        mani_pub4.publish(0.4);
                        mani_pub5.publish(0.4);
                        ros::Duration(7).sleep(); 
                        check = 20;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 20){
                        mani_pub1.publish(s1);
                        ros::Duration(5).sleep(); 
                        mani_pub4.publish(-1.0);
                        
                        check = 3;
                  }                  
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 3){
                        ros::Duration(5).sleep(); 
                        state = 2;
                        check = 4;
                        break;
                  }
      }
      case 2 : {
                  if(check == 4){
                        ROS_INFO("%.2lf  %.2lf  %.2lf  %.2lf +++++",s1.data,s2.data,s3.data,s4.data);
                        mani_pub1.publish(s1);
                        mani_pub3.publish(s3);
                        mani_pub2.publish(s2);
                        mani_pub4.publish(s4);
                        ros::Duration(0.5).sleep(); 
                        check = 5;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 5){
                        ros::Duration(5).sleep(); 
                        state = 3;
                        check = 6;
                        break;
                  }                                   
      }
      case 3 : {
                  if(check == 6){

                        mani_pub5.publish(-0.6);
                        ros::Duration(0.5).sleep(); 
                        //if(check_pos_gripper()== 0)
                        check = 7;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 7){ 
                        ros::Duration(5).sleep(); 
                        state = 4;
                        check = 8;
                        break;
                  }                                   
      }
      case 4 : {
                  if(check == 8){

                        mani_pub1.publish(1.57);
                        mani_pub3.publish(-2.07);
                        mani_pub2.publish(0.7);
                        mani_pub4.publish(0.4);
                        ros::Duration(0.5).sleep(); 
                        check = 9;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 9){ 
                        ros::Duration(5).sleep(); 
                        state = 5;
                        check = 10;
                        break;
                  }                                   
      }      
      case 5 : {
                  if(check == 10){
                        prepare_arm(se1,se2,se3,se4);
                        prepare_head(head_joint_pos0,head_joint_pos1);
                        ros::Duration(0.5).sleep(); 
                        check = 11;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 11){ 
                        ros::Duration(5).sleep(); 
                        state = 6;
                        check = 12;
                        break;
                  }                                   
      }
      case 6 : { 
           
                  break;
      }    
    }
        ros::spinOnce();


  }
}
void SpeechCallback(const speech_jv::target::ConstPtr &msg)
{
 text = *msg;
 ROS_INFO("I heard a target : [%d] [%s] [%s] [%s]", text.q ,text.place.c_str(),text.object.c_str(),text.human.c_str()); 
sound_true=1;

}
void sleepok(int t, ros::NodeHandle &n)
{
  if (n.ok())
      sleep(t);
}
void DoorCallback(const std_msgs::Bool::ConstPtr &msg)
{
  door = *msg;

}
void HalltoKitchen()
{
    state_navi = 1;
    check_navi = 2;
  while(state_navi<3)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 1 :{
        if(check_navi == 2)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 4.52;
            nav_pose.position.y = -0.64;
            nav_pose.orientation.z = 0.37;
            nav_pose.orientation.w = 0.93;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 3;
        }
        if(finish_navi == 1 && check_navi == 3)
        {
            ros::Duration(5).sleep(); 
            state_navi = 2;
            check_navi = 4;
            break;
        }
      }
      case 2 :{
        if(check_navi == 4)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.19;
            nav_pose.position.y = -0.15;
            nav_pose.orientation.z = 0.08;
            nav_pose.orientation.w = 1.0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 5;
        }
        if(finish_navi == 1 && check_navi == 5)
        {
            ros::Duration(5).sleep(); 
            state_navi = 3;
            check_navi = 6;
            break;
        }
      }
      case 3:{
        break;
      }
    
    }
   ros::spinOnce(); 
  }
}
void HalltoDiningroom()
{
    state_navi = 11;
    check_navi = 12;
  while(state_navi<12)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 11 :{
        if(check_navi == 12)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.20;
            nav_pose.position.y = 2.82;
            nav_pose.orientation.z = 0.77;
            nav_pose.orientation.w = 0.64;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 13;
        }
        if(finish_navi == 1 && check_navi == 13)
        {
            ros::Duration(5).sleep(); 
            state_navi = 12;
            check_navi = 14;
            break;
        }
      }
      case 12 :{
            break;
      }
    
    }
   ros::spinOnce(); 
  }
}
void HalltoLivingroom()
{
    state_navi = 21;
    check_navi = 22;
  while(state_navi<23)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 21 :{
        if(check_navi == 22)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.68;
            nav_pose.position.y = 3.02;
            nav_pose.orientation.z = 0.0;
            nav_pose.orientation.w = 1.0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 23;
        }
        if(finish_navi == 1 && check_navi == 23)
        {
            ros::Duration(5).sleep(); 
            state_navi = 22;
            check_navi = 24;
            break;
        }
      }
      case 22 :{
        if(check_navi == 24)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 5.22;
            nav_pose.position.y = 3.17;
            nav_pose.orientation.z = 0.34;
            nav_pose.orientation.w = 0.94;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 25;
        }
        if(finish_navi == 1 && check_navi == 25)
        {
            ros::Duration(5).sleep(); 
            state_navi = 23;
            check_navi = 26;
            break;
        }            
      }
      case 23 :{
        break;
      }
    
    }
   ros::spinOnce(); 
  }
}
void KitchentoRack()
{
    state_navi = 31;
    check_navi = 32;
  while(state_navi<33)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 31 :{
        if(check_navi == 32)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 7.03;
            nav_pose.position.y = -0.09;
            nav_pose.orientation.z = 0.37;
            nav_pose.orientation.w = 0.93;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 33;
        }
        if(finish_navi == 1 && check_navi == 33)
        {
            ros::Duration(5).sleep(); 
            state_navi = 32;
            check_navi = 34;
            break;
        }
      }
      case 32 :{
        if(check_navi == 34)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 7.90;
            nav_pose.position.y = 0.68;
            nav_pose.orientation.z = 1.0;
            nav_pose.orientation.w = 0.0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 35;
        }
        if(finish_navi == 1 && check_navi == 35)
        {
            ros::Duration(5).sleep(); 
            state_navi = 33;
            check_navi = 36;
            break;
        }
      }
      case 33 :{
        break;
      }
    
    }
   ros::spinOnce(); 
  }
}
void KitchentoTable()
{
    state_navi = 41;
    check_navi = 42;
  while(state_navi<42)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 41 :{
        if(check_navi == 42)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 5.53;
            nav_pose.position.y = 0.65;
            nav_pose.orientation.z = 1.0;
            nav_pose.orientation.w = 0.0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 43;
        }
        if(finish_navi == 1 && check_navi == 43)
        {
            ros::Duration(5).sleep(); 
            state_navi = 42;
            check_navi = 44;
            break;
        }
      }
      case 42 :{
        break;        
      }

    
    }
   ros::spinOnce(); 
  }
}
void Diningroomtolittle()
{
    state_navi = 51;
    check_navi = 52;
  while(state_navi<52)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 51 :{
        if(check_navi == 52)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 1.38;
            nav_pose.position.y = 4.85;
            nav_pose.orientation.z = -0.70;
            nav_pose.orientation.w = 0.71;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 53;
        }
        if(finish_navi == 1 && check_navi == 53)
        {
            ros::Duration(5).sleep(); 
            state_navi = 52;
            check_navi = 54;
            break;
        }
      }
      case 52 :{
        break;        
      }

    
    }
   ros::spinOnce(); 
  }
}
void DiningroomtoBar()
{
    state_navi = 61;
    check_navi = 62;
  while(state_navi<63)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 61 :{
        if(check_navi == 62)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.24;
            nav_pose.position.y = 5.53;
            nav_pose.orientation.z = 0.85;
            nav_pose.orientation.w = 0.53;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 63;
        }
        if(finish_navi == 1 && check_navi == 63)
        {
            ros::Duration(5).sleep(); 
            state_navi = 62;
            check_navi = 64;
            break;
        }
      }
      case 62 :{
        if(check_navi == 64)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 1.31;
            nav_pose.position.y = 8.34;
            nav_pose.orientation.z = -0.70;
            nav_pose.orientation.w = 0.71;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 65;
        }
        if(finish_navi == 1 && check_navi == 65)
        {
            ros::Duration(5).sleep(); 
            state_navi = 63;
            check_navi = 66;
            break;
        }
      case 63 :{
        break;
      }       
      }

    
    }
   ros::spinOnce(); 
  }
}
void DiningroomtoShelf()
{
    state_navi = 71;
    check_navi = 72;
  while(state_navi<72)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 71 :{
        if(check_navi == 72)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = -0.28;
            nav_pose.position.y = 1.96;
            nav_pose.orientation.z = 0.0;
            nav_pose.orientation.w = 1.0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 73;
        }
        if(finish_navi == 1 && check_navi == 73)
        {
            ros::Duration(5).sleep(); 
            state_navi = 72;
            check_navi = 74;
            break;
        }
      }
      case 72 :{
        if(check_navi == 74)
        {
            break;
        }
 
      }

    
    }
   ros::spinOnce(); 
  }
}
void DiningroomtoTableA()
{
    state_navi = 81;
    check_navi = 82;
  while(state_navi<82)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 81 :{
        if(check_navi == 82)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.66;
            nav_pose.position.y = 4.23;
            nav_pose.orientation.z = 0.39;
            nav_pose.orientation.w = 0.93;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 83;
        }
        if(finish_navi == 1 && check_navi == 83)
        {
            ros::Duration(5).sleep(); 
            state_navi = 82;
            check_navi = 84;
            break;
        }
      }
      case 82 :{
        if(check_navi == 84)
        {
            break;
        }
 
      }

    
    }
   ros::spinOnce(); 
  }
}
void DiningroomtoTableB()
{
    state_navi = 91;
    check_navi = 92;
  while(state_navi<93)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 91 :{
        if(check_navi == 92)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 1.90;
            nav_pose.position.y = 6.25;
            nav_pose.orientation.z = 0.47;
            nav_pose.orientation.w = 0.88;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 93;
        }
        if(finish_navi == 1 && check_navi == 93)
        {
            ros::Duration(5).sleep(); 
            state_navi = 92;
            check_navi = 94;
            break;
        }
      }
      case 92 :{
        if(check_navi == 94)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.64;
            nav_pose.position.y = 6.83;
            nav_pose.orientation.z = 0.46;
            nav_pose.orientation.w = 0.89;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 95;
        }
        if(finish_navi == 1 && check_navi == 95)
        {
            ros::Duration(5).sleep(); 
            state_navi = 93;
            check_navi = 96;
            break;
        }
 
      }
      case 93 :{
        if(check_navi == 96)
        {
            break;
        }
      }

    
    }
   ros::spinOnce(); 
  }
}
void LivingroomtoSmallshelf()
{
    state_navi = 101;
    check_navi = 102;
  while(state_navi<103)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 101 :{
        if(check_navi == 103)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.12;
            nav_pose.position.y = 4.18;
            nav_pose.orientation.z = -0.12;
            nav_pose.orientation.w = 1.0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 103;
        }
        if(finish_navi == 1 && check_navi == 103)
        {
            ros::Duration(5).sleep(); 
            state_navi = 102;
            check_navi = 104;
            break;
        }
      }
      case 102 :{
        if(check_navi == 104)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 7.48;
            nav_pose.position.y = 3.05;
            nav_pose.orientation.z = 0.0;
            nav_pose.orientation.w = 1.0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 105;
        }
        if(finish_navi == 1 && check_navi == 105)
        {
            ros::Duration(5).sleep(); 
            state_navi = 103;
            check_navi = 106;
            break;
        }
      }
      case 103 :{
        break;
      }


    
    }
   ros::spinOnce(); 
  }
}
void LivingroomtoSofa()
{
    state_navi = 111;
    check_navi = 112;
  while(state_navi<112)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 111 :{
        if(check_navi == 112)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.59;
            nav_pose.position.y = 6.29;
            nav_pose.orientation.z = 0.0;
            nav_pose.orientation.w = 1.0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 113;
        }
        if(finish_navi == 1 && check_navi == 113)
        {
            ros::Duration(5).sleep(); 
            state_navi = 112;
            check_navi = 114;
            break;
        }
      }
      case 112 :{

            break;
        
      }



    
    }
   ros::spinOnce(); 
  }
}
void LivingroomtoCoffeetable()
{
    state_navi = 121;
    check_navi = 122;
  while(state_navi<122)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 121 :{
        if(check_navi == 122)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 5.98;
            nav_pose.position.y = 8.52;
            nav_pose.orientation.z = 0.72;
            nav_pose.orientation.w = 0.69;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 123;
        }
        if(finish_navi == 1 && check_navi == 123)
        {
            ros::Duration(5).sleep(); 
            state_navi = 122;
            check_navi = 124;
            break;
        }
      }
      case 122 :{

            break;
        
      }



    
    }
   ros::spinOnce(); 
  }
}
void LivingroomtoArmchair()
{
    state_navi = 131;
    check_navi = 132;
  while(state_navi<133)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 131 :{
        if(check_navi == 132)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.29;
            nav_pose.position.y = 4.84;
            nav_pose.orientation.z = 0.76;
            nav_pose.orientation.w = 0.64;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 133;
        }
        if(finish_navi == 1 && check_navi == 133)
        {
            ros::Duration(5).sleep(); 
            state_navi = 132;
            check_navi = 134;
            break;
        }
      }
      case 132 :{
        if(check_navi == 134)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 5.66;
            nav_pose.position.y = 6.49;
            nav_pose.orientation.z = 1.0;
            nav_pose.orientation.w = 0.0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 135;
        }
        if(finish_navi == 1 && check_navi == 135)
        {
            ros::Duration(5).sleep(); 
            state_navi = 133;
            check_navi = 136;
            break;
        }
        
      }
      case 133 :{
        break;
      }



    
    }
   ros::spinOnce(); 
  }
}
void ArmchairtoLivingroom()
{
    state_navi = 141;
    check_navi = 142;
  while(state_navi<143)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 141 :{
        if(check_navi == 142)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 5.60;
            nav_pose.position.y = 5.79;
            nav_pose.orientation.z = -0.66;
            nav_pose.orientation.w = 0.75;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 143;
        }
        if(finish_navi == 1 && check_navi == 143)
        {
            ros::Duration(5).sleep(); 
            state_navi = 142;
            check_navi = 144;
            break;
        }
      }
      case 142 :{
        if(check_navi == 144)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.11;
            nav_pose.position.y = 3.58;
            nav_pose.orientation.z = 0.92;
            nav_pose.orientation.w = -0.40;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 145;
        }
        if(finish_navi == 1 && check_navi == 145)
        {
            ros::Duration(5).sleep(); 
            state_navi = 143;
            check_navi = 146;
            break;
        }
        
      }
      case 143 :{
        break;
      }



    
    }
   ros::spinOnce(); 
  }
}
void CoffeetabletoLivingroom()
{
    state_navi = 151;
    check_navi = 152;
  while(state_navi<155)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 151 :{
        if(check_navi == 152)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 5.98;
            nav_pose.position.y = 8.52;
            nav_pose.orientation.z = 0.0;
            nav_pose.orientation.w = 0.1;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 153;
        }
        if(finish_navi == 1 && check_navi == 153)
        {
            ros::Duration(5).sleep(); 
            state_navi = 152;
            check_navi = 154;
            break;
        }
      }
      case 152 :{
        if(check_navi == 154)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 7.2;
            nav_pose.position.y = 8.37;
            nav_pose.orientation.z = 0.92;
            nav_pose.orientation.w = -0.36;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 155;
        }
        if(finish_navi == 1 && check_navi == 155)
        {
            ros::Duration(5).sleep(); 
            state_navi = 153;
            check_navi = 156;
            break;
        }
        
      }
      case 153 :{
        if(check_navi == 156)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.14;
            nav_pose.position.y = 6.52;
            nav_pose.orientation.z = -0.70;
            nav_pose.orientation.w = 0.71;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 157;
        }
        if(finish_navi == 1 && check_navi == 157)
        {
            ros::Duration(5).sleep(); 
            state_navi = 154;
            check_navi = 158;
            break;
        }
      }
      case 154 :{
        if(check_navi == 158)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.11;
            nav_pose.position.y = 3.58;
            nav_pose.orientation.z = 0.92;
            nav_pose.orientation.w = -0.40;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 159;
        }
        if(finish_navi == 1 && check_navi == 159)
        {
            ros::Duration(5).sleep(); 
            state_navi = 155;
            check_navi = 160;
            break;
        }
      }
      case 155:{
        break;
      }



    
    }
   ros::spinOnce(); 
  }
}
void SofatoLivingroom()
{
    state_navi = 161;
    check_navi = 162;
  while(state_navi<163)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 161 :{
        if(check_navi == 162)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.59;
            nav_pose.position.y = 6.29;
            nav_pose.orientation.z = -0.7;
            nav_pose.orientation.w = 0.71;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 163;
        }
        if(finish_navi == 1 && check_navi == 163)
        {
            ros::Duration(5).sleep(); 
            state_navi = 162;
            check_navi = 164;
            break;
        }
      }
      case 162 :{
        if(check_navi == 164)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.11;
            nav_pose.position.y = 3.58;
            nav_pose.orientation.z = 0.92;
            nav_pose.orientation.w = -0.40;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 165;
        }
        if(finish_navi == 1 && check_navi == 165)
        {
            ros::Duration(5).sleep(); 
            state_navi = 163;
            check_navi = 166;
            break;
        }
        
      }
      case 163 :{
        break;
      }



    
    }
   ros::spinOnce(); 
  }
}
void SmallshelftoLivingroom()
{
    state_navi = 171;
    check_navi = 172;
  while(state_navi<174)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 171 :{
        if(check_navi == 172)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 7.48;
            nav_pose.position.y = 3.05;
            nav_pose.orientation.z = 0.70;
            nav_pose.orientation.w = 0.71;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 173;
        }
        if(finish_navi == 1 && check_navi == 173)
        {
            ros::Duration(5).sleep(); 
            state_navi = 172;
            check_navi = 174;
            break;
        }
      }
      case 172 :{
        if(check_navi == 174)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.67;
            nav_pose.position.y = 4.15;
            nav_pose.orientation.z = 0.93;
            nav_pose.orientation.w = -0.36;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 175;
        }
        if(finish_navi == 1 && check_navi == 175)
        {
            ros::Duration(5).sleep(); 
            state_navi = 173;
            check_navi = 176;
            break;
        }
        
      }
      case 173 :{
        if(check_navi == 176)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.11;
            nav_pose.position.y = 3.58;
            nav_pose.orientation.z = 0.92;
            nav_pose.orientation.w = -0.40;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 177;
        }
        if(finish_navi == 1 && check_navi == 177)
        {
            ros::Duration(5).sleep(); 
            state_navi = 174;
            check_navi = 178;
            break;
        }
      }
      case 174 :{
        break;
      }



    
    }
   ros::spinOnce(); 
  }
}
void TableBtoDiningroom()
{
    state_navi = 181;
    check_navi = 182;
  while(state_navi<184)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 181 :{
        if(check_navi == 182)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.64;
            nav_pose.position.y = 6.83;
            nav_pose.orientation.z = -0.43;
            nav_pose.orientation.w = 0.90;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 183;
        }
        if(finish_navi == 1 && check_navi == 183)
        {
            ros::Duration(5).sleep(); 
            state_navi = 182;
            check_navi = 184;
            break;
        }
      }
      case 182 :{
        if(check_navi == 184)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.64;
            nav_pose.position.y = 6.83;
            nav_pose.orientation.z = 0.86;
            nav_pose.orientation.w = -0.50;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 185;
        }
        if(finish_navi == 1 && check_navi == 185)
        {
            ros::Duration(5).sleep(); 
            state_navi = 183;
            check_navi = 186;
            break;
        }
 
      }
      case 183 :{
        if(check_navi == 186)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.21;
            nav_pose.position.y = 3.83;
            nav_pose.orientation.z = -0.72;
            nav_pose.orientation.w = 0.69;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 187;
        }
        if(finish_navi == 1 && check_navi == 187)
        {
            ros::Duration(5).sleep(); 
            state_navi = 184;
            check_navi = 188;
            break;
        }
      }
      case 184 :{
        break;
      }

    
    }
   ros::spinOnce(); 
  }
}
void TableAtoDiningroom()
{
    state_navi = 191;
    check_navi = 192;
  while(state_navi<194)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 191 :{
        if(check_navi == 192)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.66;
            nav_pose.position.y = 4.23;
            nav_pose.orientation.z = -0.60;
            nav_pose.orientation.w = 0.80;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 193;
        }
        if(finish_navi == 1 && check_navi == 193)
        {
            ros::Duration(5).sleep(); 
            state_navi = 192;
            check_navi = 194;
            break;
        }
      }
      case 192 :{
        if(check_navi == 194)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.64;
            nav_pose.position.y = 6.83;
            nav_pose.orientation.z = 0.87;
            nav_pose.orientation.w = -0.50;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 195;
        }
        if(finish_navi == 1 && check_navi == 195)
        {
            ros::Duration(5).sleep(); 
            state_navi = 193;
            check_navi = 196;
            break;
        }
 
      }
      case 193 :{
        if(check_navi == 196)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.21;
            nav_pose.position.y = 3.83;
            nav_pose.orientation.z = -0.72;
            nav_pose.orientation.w = 0.69;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 197;
        }
        if(finish_navi == 1 && check_navi == 197)
        {
            ros::Duration(5).sleep(); 
            state_navi = 194;
            check_navi = 198;
            break;
        }
      }
      case 194 :{
        break;
      }


    
    }
   ros::spinOnce(); 
  }
}
void BartoDiningroom()
{
    state_navi = 201;
    check_navi = 202;
  while(state_navi<203)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 201 :{
        if(check_navi == 202)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.03;
            nav_pose.position.y = 6.71;
            nav_pose.orientation.z = -0.70;
            nav_pose.orientation.w = 0.71;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 203;
        }
        if(finish_navi == 1 && check_navi == 203)
        {
            ros::Duration(5).sleep(); 
            state_navi = 202;
            check_navi = 204;
            break;
        }
      }
      case 202 :{
        if(check_navi == 204)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.21;
            nav_pose.position.y = 3.83;
            nav_pose.orientation.z = -0.72;
            nav_pose.orientation.w = 0.69;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 205;
        }
        if(finish_navi == 1 && check_navi == 205)
        {
            ros::Duration(5).sleep(); 
            state_navi = 203;
            check_navi = 206;
            break;
        }
 
      }
      case 203 :{
          break;
      }


    
    }
   ros::spinOnce(); 
  }
}
void LittletoDiningroom()
{
    state_navi = 211;
    check_navi = 212;
  while(state_navi<212)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 211 :{
        if(check_navi == 212)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.21;
            nav_pose.position.y = 3.83;
            nav_pose.orientation.z = -0.72;
            nav_pose.orientation.w = 0.69;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 213;
        }
        if(finish_navi == 1 && check_navi == 213)
        {
            ros::Duration(5).sleep(); 
            state_navi = 212;
            check_navi = 214;
            break;
        }
      }
      case 212 :{
          break;
      }
 
    }
   ros::spinOnce(); 
  }
}
void ShelftoDiningroom()
{
    state_navi = 221;
    check_navi = 222;
  while(state_navi<222)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 221 :{
        if(check_navi == 222)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.21;
            nav_pose.position.y = 3.83;
            nav_pose.orientation.z = -0.72;
            nav_pose.orientation.w = 0.69;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 223;
        }
        if(finish_navi == 1 && check_navi == 213)
        {
            ros::Duration(5).sleep(); 
            state_navi = 222;
            check_navi = 224;
            break;
        }
      }
      case 222 :{
          break;
      }
 
    }
   ros::spinOnce(); 
  }
}
void RacktoKitchen()
{
    state_navi = 231;
    check_navi = 232;
  while(state_navi<232)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 231 :{
        if(check_navi == 232)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 6.5;
            nav_pose.position.y = -0.17;
            nav_pose.orientation.z = 1;
            nav_pose.orientation.w = 0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 233;
        }
        if(finish_navi == 1 && check_navi == 233)
        {
            ros::Duration(5).sleep(); 
            state_navi = 232;
            check_navi = 234;
            break;
        }
      }
      case 232 :{
        break;
      }

    
    }
   ros::spinOnce(); 
  }
}
void TabletoKitchen()
{
    state_navi = 241;
    check_navi = 242;
  while(state_navi<242)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 241 :{
        if(check_navi == 242)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 4.98;
            nav_pose.position.y = -0.33;
            nav_pose.orientation.z = 1;
            nav_pose.orientation.w = 0;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 243;
        }
        if(finish_navi == 1 && check_navi == 243)
        {
            ros::Duration(5).sleep(); 
            state_navi = 242;
            check_navi = 244;
            break;
        }
      }
      case 242 :{
        break;
      }

    
    }
   ros::spinOnce(); 
  }
}
void KitchentoHall()
{
    state_navi = 251;
    check_navi = 252;
  while(state_navi<252)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 251 :{
        if(check_navi == 252)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 2.07;
            nav_pose.position.y = -0.21;
            nav_pose.orientation.z = 0.63;
            nav_pose.orientation.w = 0.78;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 253;
        }
        if(finish_navi == 1 && check_navi == 253)
        {
            ros::Duration(5).sleep(); 
            state_navi = 252;
            check_navi = 254;
            break;
        }
      }
      case 252 :{
        break;
      }

    
    }
   ros::spinOnce(); 
  }
}
void LivingroomtoHall()
{
    state_navi = 261;
    check_navi = 262;
  while(state_navi<263)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 261 :{
        if(check_navi == 262)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 3.02;
            nav_pose.position.y = 2.93;
            nav_pose.orientation.z = 0.89;
            nav_pose.orientation.w = -0.46;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 263;
        }
        if(finish_navi == 1 && check_navi == 263)
        {
            ros::Duration(5).sleep(); 
            state_navi = 262;
            check_navi = 264;
            break;
        }
      }
      case 262 :{
        if(check_navi == 264)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 1.62;
            nav_pose.position.y = 0.25;
            nav_pose.orientation.z = 0.51;
            nav_pose.orientation.w = 0.86;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 265;
        }
        if(finish_navi == 1 && check_navi == 265)
        {
            ros::Duration(5).sleep(); 
            state_navi = 263;
            check_navi = 266;
            break;
        }
      }
      case 263 :{
        break;
      }

    
    }
   ros::spinOnce(); 
  }
}
void DiningroomtoHall()
{
    state_navi = 271;
    check_navi = 272;
  while(state_navi<272)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 271 :{
        if(check_navi == 272)
        {
            system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
            ros::Duration(5.0).sleep(); 
            nav_pose.position.x = 1.62;
            nav_pose.position.y = 0.304;
            nav_pose.orientation.z = 0.48;
            nav_pose.orientation.w = 0.88;
            navi_pub.publish(nav_pose); 
            finish_navi =0;
            check_navi = 273;
        }
        if(finish_navi == 1 && check_navi == 273)
        {
            ros::Duration(5).sleep(); 
            state_navi = 272;
            check_navi = 274;
            break;
        }
      }
      case 272 :{
        break;
      }

    }
   ros::spinOnce(); 
  }
}
int main(int argc, char **argv)
{

  ros::init(argc, argv, "gpsr_rcap2017");

 
  ros::NodeHandle n;
  sound_play::SoundClient sc;

  sub = n.subscribe("/joy", 1, &JoyCallback);
  sub2 = n.subscribe("/navi_finish", 1000, &NavCallback);
  sub3 = n.subscribe("/pub_to_main/object_detection_list", 1000, &ImageCallback);
  navi_pub  = n.advertise<geometry_msgs::Pose>("/nav_goal",1000);
  chatter_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1000);
  mani_pub1  = n.advertise<std_msgs::Float64>("arm0_controller/command",1000);  
  mani_pub2  = n.advertise<std_msgs::Float64>("arm1_controller/command",1000);
  mani_pub3  = n.advertise<std_msgs::Float64>("arm2_controller/command",1000);  
  mani_pub4  = n.advertise<std_msgs::Float64>("arm3_controller/command",1000);
  mani_pub5  = n.advertise<std_msgs::Float64>("arm4_controller/command",1000);
  head0_pub = n.advertise<std_msgs::Float64>("head0_controller/command",1000);
  head1_pub = n.advertise<std_msgs::Float64>("head1_controller/command",1000);
  simhead0_pub = n.advertise<std_msgs::Float64>("head0",1000);
  simhead1_pub = n.advertise<std_msgs::Float64>("head1",1000);
  feedback_arm0 = n.subscribe("arm0_controller/state", 1000, arm0Callback);
  feedback_arm1 = n.subscribe("arm1_controller/state", 1000, arm1Callback);
  feedback_arm2 = n.subscribe("arm2_controller/state", 1000, arm2Callback);
  feedback_arm3 = n.subscribe("arm3_controller/state", 1000, arm3Callback);
  feedback_arm4 = n.subscribe("arm4_controller/state", 1000, arm4Callback);
  feedback_head0 = n.subscribe("head0_controller/state", 1000, head0Callback);
  feedback_head1 = n.subscribe("head1_controller/state", 1000, head1Callback);
  sub_speech_ = n.subscribe("/Target", 1,SpeechCallback);
  door_sub = n.subscribe("/pub_to_main/door",1,DoorCallback);
  //hark_sub = n.subscribe("/hark_source",1000, harkCallback);
  ros::Rate loop_rate(10);


  while (ros::ok())
  {
    prepare_arm(se1,se2,se3,se4);
    prepare_head(head_joint_pos0,head_joint_pos1);

     if(JOY_BUTTON[JOY_BUTTON_A] == 1)
   {
    text.place = " ";
    system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
    // system("x-terminal-emulator -e rosrun urg_node urg_node");
    ros::Duration(5.0).sleep(); 
    nav_pose.position.x = 1.0;
    nav_pose.position.y = 0.0;
    nav_pose.orientation.z = 0.0;
    nav_pose.orientation.w = 1.0;
    navi_pub.publish(nav_pose);
   }
    else if (JOY_BUTTON[JOY_BUTTON_X] == 1)
    {
      HalltoKitchen();
      KitchentoTable();
    }    
    else if (JOY_BUTTON[JOY_BUTTON_B] == 1)
    {
      HalltoDiningroom();
      DiningroomtoShelf();
    }








      
    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
   //   if(JOY_BUTTON[JOY_BUTTON_A] == 1)
   // {
   //  system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
   //  // system("x-terminal-emulator -e rosrun urg_node urg_node");
   //  ros::Duration(5.0).sleep(); 
   //  ROS_INFO("11111111111111111111");
   //  nav_pose.position.x = 1.00;
   //  nav_pose.position.y = 0.95;
   //  nav_pose.orientation.z = -0.70;
   //  nav_pose.orientation.w = 0.71;
   //  navi_pub.publish(nav_pose);
   // }
   //  else if(JOY_BUTTON[JOY_BUTTON_X] == 1)
   // {
   //  system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
   //  // system("x-terminal-emulator -e rosrun urg_node urg_node");
   //  ros::Duration(5.0).sleep(); 
   //  ROS_INFO("22222222222222222222");
   //  nav_pose.position.x = -0.22;
   //  nav_pose.position.y = -2.09;
   //  nav_pose.orientation.z = 0.00;
   //  nav_pose.orientation.w = 1.00;
   //  navi_pub.publish(nav_pose);
   // }
   //  else if(JOY_BUTTON[JOY_BUTTON_B] == 1)
   // {
   //  system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
   //  // system("x-terminal-emulator -e rosrun urg_node urg_node");
   //  ros::Duration(5.0).sleep(); 
   //  ROS_INFO("33333333333333333333");
   //  nav_pose.position.x = -0.62;
   //  nav_pose.position.y = -4.6;
   //  nav_pose.orientation.z = -0.69;
   //  nav_pose.orientation.w = 0.72;
   //  navi_pub.publish(nav_pose);
   // }
   //  else if(JOY_BUTTON[JOY_BUTTON_Y] == 1)
   // {
   //  system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
   //  // system("x-terminal-emulator -e rosrun urg_node urg_node");
   //  ros::Duration(5.0).sleep(); 
   //  nav_pose.position.x = 0.58;
   //  nav_pose.position.y = 0;
   //  nav_pose.orientation.z = -1;
   //  nav_pose.orientation.w = 0;
   //  navi_pub.publish(nav_pose);

   // }    


// prepare_arm(se1,se2,se3,se4);
//     prepare_head(head_joint_pos0,head_joint_pos1);
//      if("kitchen" == text.place && state_navi == 0)
//    {
//     system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
//     // system("x-terminal-emulator -e rosrun urg_node urg_node");
//     ros::Duration(5.0).sleep(); 
//     ROS_INFO("11111111111111111111");
//     nav_pose.position.x = 1.00;
//     nav_pose.position.y = 0.95;
//     nav_pose.orientation.z = -0.70;
//     nav_pose.orientation.w = 0.71;
//     navi_pub.publish(nav_pose);
//     finish_navi = 0;
//     state_navi = 0;
//    }
//     else if("bathroom" == text.place && state_navi == 0)
//    {
//     system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
//     // system("x-terminal-emulator -e rosrun urg_node urg_node");
//     ros::Duration(5.0).sleep(); 
//     ROS_INFO("22222222222222222222");
//     nav_pose.position.x = -0.35;
//     nav_pose.position.y = -2.09;
//     nav_pose.orientation.z = 0.00;
//     nav_pose.orientation.w = 1.00;
//     navi_pub.publish(nav_pose);
//     finish_navi = 0;
//     state_navi = 0;
//    }
//     else if("living" == text.place && state_navi == 0 )
//    {
//     system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
//     // system("x-terminal-emulator -e rosrun urg_node urg_node");
//     ros::Duration(5.0).sleep(); 
//     ROS_INFO("22222222222222222222");
//     nav_pose.position.x = -0.4;
//     nav_pose.position.y = -2.09;
//     nav_pose.orientation.z = -0.48;
//     nav_pose.orientation.w = 0.87;
//     navi_pub.publish(nav_pose);
//     finish_navi = 0;
//     state_navi = 1;
//    }
//     else if("living" == text.place && state_navi == 1 && finish_navi == 1)
//    {
//     system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
//     // system("x-terminal-emulator -e rosrun urg_node urg_node");
//     ros::Duration(5.0).sleep(); 
//     ROS_INFO("33333333333333333333");
//     nav_pose.position.x = -0.65;
//     nav_pose.position.y = -4.16;
//     nav_pose.orientation.z = -0.72;
//     nav_pose.orientation.w = 0.69;
//     navi_pub.publish(nav_pose);
//     finish_navi = 0;
//     state_navi = 0;
//    }
//     else if("bedroom" == text.place && state_navi == 0)
//    {
//     system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
//     // system("x-terminal-emulator -e rosrun urg_node urg_node");
//     ros::Duration(5.0).sleep(); 
//     nav_pose.position.x = -0.60;
//     nav_pose.position.y = -4.07;
//     nav_pose.orientation.z = 0.27;
//     nav_pose.orientation.w = 0.96;
//     navi_pub.publish(nav_pose);  
//     finish_navi =0;
//     state_navi = 1;
//    }  
//     else if("bedroom" == text.place && state_navi == 1 && finish_navi == 1)
//    {
//     system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
//     // system("x-terminal-emulator -e rosrun urg_node urg_node");
//     ros::Duration(5.0).sleep(); 
//     nav_pose.position.x = -0.16;
//     nav_pose.position.y = -3.01;
//     nav_pose.orientation.z = 0.14;
//     nav_pose.orientation.w = 0.98;
//     navi_pub.publish(nav_pose);
//     finish_navi = 0;
//     state_navi = 2;
//    }  
//     else if("bedroom" == text.place && state_navi == 2 && finish_navi == 1)
//    {
//     system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
//     // system("x-terminal-emulator -e rosrun urg_node urg_node");
//     ros::Duration(5.0).sleep(); 
//     nav_pose.position.x = 0.70;
//     nav_pose.position.y = -2.74;
//     nav_pose.orientation.z = 0.43;
//     nav_pose.orientation.w = 0.90;
//     navi_pub.publish(nav_pose);
//     finish_navi = 0;
//     state_navi = 3;
//    }     
//     else if("bedroom" == text.place && state_navi == 3 && finish_navi == 1)
//    {
//     system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
//     // system("x-terminal-emulator -e rosrun urg_node urg_node");
//     ros::Duration(5.0).sleep(); 
//     nav_pose.position.x = 0.58;
//     nav_pose.position.y = 0;
//     nav_pose.orientation.z = -1;
//     nav_pose.orientation.w = 0;
//     navi_pub.publish(nav_pose);
//     finish_navi =0;
//     state_navi = 0;
//    }    

   //  ROS_INFO("%d  %d  %d  %d",m_state,m_check,state,check);
   //  if(JOY_BUTTON[JOY_BUTTON_A] == 1)
   // {
   //    m_state = 1;
   //    m_check = 2; 
   // }
   //    switch(m_state)
   //    {
   //            case 0 :{
   //                        prepare_arm(se1,se2,se3,se4);
   //                        prepare_head(head_joint_pos0,head_joint_pos1);

   //            }
   //            case 1 : {
   //                if(m_check == 2){
   //                      head_scan(head_joint_pos0,head_joint_pos1); 
   //                      system("x-terminal-emulator -e roslaunch darknet_ros darknet_ros.launch");
   //                      ros::Duration(10).sleep(); 
   //                      // if(check_pos_head() == 0)
   //                      m_check = 3;
   //                }
   //                if(check_pos_arm() == 1 && check_pos_head() == 1 && m_check == 3){
   //                      ros::Duration(5).sleep(); 
   //                      m_state = 2;
   //                      m_check = 4;
   //                      break;
   //                }
   //            }
   //            case 2 : {
   //                if(m_check == 4){
   //                      // system("x-terminal-emulator -e roslaunch darknet_ros darknet_ros.launch");
   //                      // system("x-terminal-emulator -e roslaunch openni_launch openni.launch");
   //                      //ros::Duration(10).sleep(); 
   //                      inverse_kinematic(point,se1,se2,se3,se4);
   //                      m_check = 5;
   //                }

   //                if(check_inverse == 1 && m_check == 5){
   //                      ros::Duration(5).sleep(); 
   //                      m_state = 3;
   //                      m_check = 6;
   //                      system("rosnode kill /darknet_ros");
   //                      break;
   //                }
   //                else if(check_inverse == 10 && m_check == 5){
   //                      check_inverse = 0;
   //                      m_state = 2;
   //                      m_check = 4;
   //                }

   //            }
   //            case 3 : {
   //                if(m_check == 6){
                        
   //                      manipulation(se1,se2,se3,se4);
   //                      if(state != 100 && check != 100)
   //                      m_check = 7;
   //                }
   //                if(state == 6 && check == 12 && m_check == 7){
   //                      ros::Duration(5).sleep(); 
   //                      m_state = 4;
   //                      m_check = 8;
                        
   //                      break;
   //                }
   //            }
   //            case 4 : {

   //                        break;

   //            }                          
   //    }

      //    ROS_INFO("%d",m_state);
      // switch(m_state)
      // {
      //   case 0: //ยกแขน
      //   {
      //     prepare_arm(se1,se2,se3,se4);
      //     prepare_head(head_joint_pos0,head_joint_pos1);
      //     if(door.data)
      //       m_state++;
      //     break;
      //   }
      //   case 1: //กลางห้อง
      //   {
      //     system("x-terminal-emulator -e roslaunch jarvis carto_mapping_movebase.launch");
      //     ros::Duration(5.0).sleep(); 
      //     nav_pose.position.x = 1.5;
      //     nav_pose.position.y = 0.0;
      //     nav_pose.orientation.z = 0.0;
      //     nav_pose.orientation.w = 1.0;
      //     navi_pub.publish(nav_pose);
      //     finish_navi =0;
      //     m_state++;
      //     break;
      //   }
      //   case 2: 
      //   {
      //     if(finish_navi == 1)
      //       m_state++;
      //     break;
      //   }
      //   case 3: //sound
      //   {
      //     HalltoKitchen();
      //     m_state++;
      //     finish_navi=0;
      //     break;
      //   }


      //   case 4: //sound
      //   {
      //     if(sound_true)
      //       m_state++;
      //     break;
      //   }





      //   case 5: //ที่1
      //   {
      //     if(text.place=="kitchen table"||text.place=="kitchen rack")
      //       {

      //         if(text.place=="kitchen table")
      //           {
      //             KitchentoTable();
      //           }
      //         else if(text.place=="kitchen rack")
      //           {
      //             KitchentoRack();
      //           }
      //       } 
      //     else if(text.place=="shelf"||text.place=="little table"||text.place=="bar table"||text.place=="table a"||text.place=="table b")
      //       {
      //         KitchentoHall();
      //         HalltoDiningroom();
      //         if (text.place=="shelf")
      //         {
      //           DiningroomtoShelf();
      //         }
      //         else if (text.place=="little table")
      //         {
      //           Diningroomtolittle();
      //         }
      //          else if (text.place=="bar table")
      //         {
      //           DiningroomtoBar();
      //         }

      //         else if (text.place=="table a")
      //         {
      //           DiningroomtoTableA();
      //         }
      //         else if (text.place=="table b")
      //         {
      //           DiningroomtoTableB();
      //         }
      //       }
      //     else if(text.place=="small shelf"||text.place=="sofa"||text.place=="coffee table"||text.place=="arm chair")
      //       {
      //         KitchentoHall();
      //         HalltoLivingroom();
      //         if (text.place=="small shelf")
      //         {
      //           LivingroomtoSmallshelf();
      //         }
      //         else if (text.place=="sofa")
      //         {
      //           LivingroomtoSofa();
      //         }
      //          else if (text.place=="coffee table")
      //         {
      //           LivingroomtoCoffeetable();
      //         }

      //         else if (text.place=="arm chair")
      //         {
      //           LivingroomtoArmchair();
      //         }
      //       }
      //     ROS_INFO("Where");
      //     finish_navi=0;
      //     m_state++;
      //     break;
      //   }

      //   case 6: //darknet
      //   {
      //     head_scan(head_joint_pos0,head_joint_pos1); 
      //     ros::Duration(1).sleep();
      //     system("x-terminal-emulator -e roslaunch darknet_ros darknet_ros.launch");
      //     ros::Duration(10).sleep();

      //     m_state++;
      //     break;
      //   }
      //   case 7: 
      //   {
      //     if(object_true)
      //       m_state++;
      //     break;
      //   }
      //   case 8: //IK
      //   {
      //     inverse_kinematic(point,se1,se2,se3,se4);
      //     if (check_inverse==10)
      //     {
      //       // inverse_kinematic(point,se1,se2,se3,se4);
      //       check_inverse=0;
            
      //     }
      //     else if(check_inverse==1)
      //     {
      //       system("rosnode kill /darknet_ros");
      //       m_state++;
      //     }
      //     break;
      //   }

      //   case 9: 
      //   {
      //     manipulation(se1,se2,se3,se4);
      //     m_state++;
      //     break;
      //   }

      //   case 10: //ที่2
      //   {




      //     finish_navi =0;
      //     m_state++;
      //     break;
      //   }
      //   case 11: 
      //   {
      //     if(finish_navi == 1)
      //       m_state++;
      //     break;
      //   }

      //   case 12: //ที่2
      //   {
      //     if(text.place=="kitchen table"||text.place=="kitchen rack")
      //       {
      //         if(text.place=="kitchen table")
      //           {
      //             TabletoKitchen();
      //           }
      //         else if(text.place=="kitchen rack")
      //           {
      //             RacktoKitchen();
      //           }
      //         KitchentoHall();
      //       } 
      //     else if(text.place=="shelf"||text.place=="little table"||text.place=="bar table"||text.place=="table a"||text.place=="table b")
      //       {
      //         if (text.place=="shelf")
      //         {
      //           ShelftoDiningroom();
      //         }
      //         else if (text.place=="little table")
      //         {
      //           LittletoDiningroom();
      //         }
      //          else if (text.place=="bar table")
      //         {
      //           BartoDiningroom();
      //         }

      //         else if (text.place=="table a")
      //         {
      //           TableAtoDiningroom();
      //         }
      //         else if (text.place=="table b")
      //         {
      //           TableBtoDiningroom();
      //         }
      //         DiningroomtoHall();
      //       }
      //     else if(text.place=="small shelf"||text.place=="sofa"||text.place=="coffee table"||text.place=="arm chair")
      //       {
      //         if (text.place=="small shelf")
      //         {
      //           SmallshelftoLivingroom();
      //         }
      //         else if (text.place=="sofa")
      //         {
      //           SofatoLivingroom();
      //         }
      //          else if (text.place=="coffee table")
      //         {
      //           CoffeetabletoLivingroom();
      //         }

      //         else if (text.place=="arm chair")
      //         {
      //           ArmchairtoLivingroom();
      //         }
      //         LivingroomtoHall();

      //       }
      //       if(text.human=="me"||text.human=="kitchen")
      //       {
      //         HalltoKitchen();
      //       }
      //       else if(text.human=="dining room")
      //       {
      //         HalltoDiningroom();
      //       }
      //       else if(text.human=="living room")
      //       {
      //         HalltoLivingroom();
      //       }

      //     ROS_INFO("Where2");
      //     finish_navi=0;
      //     m_state++;
      //     break;
      //   }

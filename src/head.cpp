#include <ros/ros.h>
#include <math.h>
#include <std_msgs/Float64.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <dynamixel_msgs/JointState.h>

std_msgs::Float64 head_joint_pos0;
std_msgs::Float64 head_joint_pos1;
void head0Callback(const dynamixel_msgs::JointState& msg)
{
  head_joint_pos0.data = msg.current_pos;
}
void head1Callback(const dynamixel_msgs::JointState& msg)
{
  head_joint_pos1.data = msg.current_pos;
}
int main(int argc, char **argv)
{

  ros::init(argc, argv, "head");

 
  ros::NodeHandle n;

  ros::Subscriber sub_head0 = n.subscribe("head0_controller/state", 1000, &head0Callback);
  ros::Subscriber sub_head1 = n.subscribe("head1_controller/state", 1000, &head1Callback);
  ros::Rate loop_rate(10);


  while (ros::ok())
  {
    static tf::TransformBroadcaster head_pan_br, head_tilt_br, br,image_br;
    tf::Transform transform;
    tf::Quaternion q;

    q.setRPY(0,0,head_joint_pos0.data);
    transform.setOrigin(tf::Vector3(0.16,0,1.16));
    transform.setRotation(q);
    head_pan_br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "head_pan_link"));

    q.setRPY(0,head_joint_pos1.data,0);
    transform.setOrigin(tf::Vector3(0,0,0));
    transform.setRotation(q);
    head_tilt_br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "head_pan_link", "head_tilt_link"));



    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
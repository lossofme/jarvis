#include <ros/ros.h>
#include <math.h>
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <geometry_msgs/Transform.h>
#include "std_msgs/Float32.h"
#include <std_msgs/Float64.h>
#include <tf/transform_broadcaster.h>
#include <dynamixel_msgs/JointState.h>

#define PI 3.1415926535897932384626433
#define ACCEPT_ERROR 0.01
#define ACCEPT_ERROR2 0.04
geometry_msgs::Transform point;
sensor_msgs::JointState joint;
double arm0,arm1,arm2,arm3,arm4,head0,head1;
ros::Publisher chatter_pub,mani_pub1,mani_pub2,mani_pub3,mani_pub4,mani_pub5,head0_pub,head1_pub;
ros::Subscriber feedback_arm0,feedback_arm1,feedback_arm2,feedback_arm3,feedback_arm4,feedback_head0,feedback_head1;
double px,py,pz,sea,seo,sen,nx,ny,nz,ox,oy,oz,ax,ay,az,a2,a3;

int state = 0;
int check = 0;
std_msgs::Float64 se1;
std_msgs::Float64 se2;
std_msgs::Float64 se3;
std_msgs::Float64 se4;
std_msgs::Float64 se5;
std_msgs::Float64 head_joint_pos0;
std_msgs::Float64 head_joint_pos1;


double csc(double x)
{
  return 1/sin(x);
}

double cot(double x)
{
  return 1/tan(x);
}
void arm0Callback(const dynamixel_msgs::JointState& a0)
{
  arm0 = abs(a0.error);
}
void arm1Callback(const dynamixel_msgs::JointState& a1)
{
  arm1 = abs(a1.error);
}
void arm2Callback(const dynamixel_msgs::JointState& a2)
{
  arm2 = abs(a2.error);
}
void arm3Callback(const dynamixel_msgs::JointState& a3)
{
  arm3 = abs(a3.error);
}
void arm4Callback(const dynamixel_msgs::JointState& a4)
{
  arm4 = abs(a4.error);
}
void head0Callback(const dynamixel_msgs::JointState& h0)
{
  head0 = abs(h0.error);
}
void head1Callback(const dynamixel_msgs::JointState& h1)
{
  head1 = abs(h1.error);
}
int check_pos_arm()
{
  if(arm0 < ACCEPT_ERROR && arm1 < ACCEPT_ERROR && arm2 < ACCEPT_ERROR && arm3 < ACCEPT_ERROR)
    return 1;
  else
    return 0;
}
int check_pos_head()
{
  if(head0 < ACCEPT_ERROR && head1 < ACCEPT_ERROR)
    return 1;
  else
    return 0;
}
int check_pos_gripper()
{
  if(arm4 < ACCEPT_ERROR2)
    return 1;
  else
    return 0;
}
// void transformCallback(const geometry_msgs::Transform& msg)
// {
// 	point.translation.x = msg.translation.x;
// 	point.translation.y = msg.translation.y;
// 	point.translation.z = msg.translation.z;
// 	point.rotation.x = msg.rotation.x;
// 	point.rotation.y = msg.rotation.y;
// 	point.rotation.z = msg.rotation.z;
// }
int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "inversejarvis");

  
  ros::NodeHandle n;
  chatter_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1000);
  mani_pub1  = n.advertise<std_msgs::Float64>("arm0_controller/command",1000);  
  mani_pub2  = n.advertise<std_msgs::Float64>("arm1_controller/command",1000);
  mani_pub3  = n.advertise<std_msgs::Float64>("arm2_controller/command",1000);  
  mani_pub4  = n.advertise<std_msgs::Float64>("arm3_controller/command",1000);
  mani_pub5  = n.advertise<std_msgs::Float64>("arm4_controller/command",1000);
  head0_pub = n.advertise<std_msgs::Float64>("head0_controller/command",1000);
  head1_pub = n.advertise<std_msgs::Float64>("head1_controller/command",1000);
  feedback_arm0 = n.subscribe("arm0_controller/state", 1000, arm0Callback);
  feedback_arm1 = n.subscribe("arm1_controller/state", 1000, arm1Callback);
  feedback_arm2 = n.subscribe("arm2_controller/state", 1000, arm2Callback);
  feedback_arm3 = n.subscribe("arm3_controller/state", 1000, arm3Callback);
  feedback_arm4 = n.subscribe("arm4_controller/state", 1000, arm4Callback);
  feedback_head0 = n.subscribe("head0_controller/state", 1000, head0Callback);
  feedback_head1 = n.subscribe("head1_controller/state", 1000, head1Callback);
   // tf::TransformBroadcaster br;
   // tf::Transform transform;
  ros::Rate loop_rate(10);




   a2 = 0.175;
   a3 = 0.1702;
  // pre-pick 1.57 0.7 -2.07 0.4
  // prepare 0.0 -1.2 -1.6 1.0

    // px = 0;   
    // py = -a3-0.02;
    // pz = 0.2;
    px = 0;
    py = -0.3;
    pz = 0.0;
    sea = 0;
    seo = PI/2;
    sen = 0;

  while (ros::ok())
  {
    static tf::TransformBroadcaster head_pan_br, head_tilt_br;
    tf::Transform transform;
    tf::Quaternion q;
  	
      q.setRPY(0,0,head_joint_pos0.data);
    transform.setOrigin(tf::Vector3(0.16,0,1.16));
    transform.setRotation(q);

    head_pan_br.sendTransform(
      tf::StampedTransform(
        transform, ros::Time::now(), "base_link", "head_pan_link"));
    q.setRPY(0,-head_joint_pos1.data,0);
    transform.setOrigin(tf::Vector3(0,0,0));
    transform.setRotation(q);

    head_tilt_br.sendTransform(
      tf::StampedTransform(
        transform, ros::Time::now(), "head_pan_link", "head_tilt_link"));
  	// px = point.translation.x;
  	// py = point.translation.y; 
   //  pz = point.translation.z; 
 	 //  sea = point.rotation.x;
   //  seo = point.rotation.y;
   //  sen = point.rotation.z;
   


// int xxxxx =scanf("%lf %lf %lf ",&px,&py,&pz);
	nx = cos(sea)*cos(seo);
	ny = cos(seo)*sin(sea);
	nz = -sin(seo);
	ox = (-cos(sen)*sin(sea))+(cos(sea)*sin(sen)*sin(seo));
	oy = (cos(sea)*cos(sen))+(sin(sea)*sin(sen)*sin(seo));
	oz = cos(seo)*sin(sen);
	ax = (sin(sea)*sin(sen))+(cos(sea)*cos(sen)*sin(seo));
	ay = (-cos(sea)*sin(sen))+(cos(sen)*sin(sea)*sin(seo));
	az = cos(sen)*cos(seo);

	se1.data = atan2((py/(sqrt(pow(px,2)+pow(py,2)))),(px/(sqrt(pow(px,2)+pow(py,2)))));
	se3.data = -acos((-pow(a2,2)-pow(a3,2)+pow(px,2)+pow(py,2)+pow(pz,2))/(2*a2*a3));
  se2.data = atan2(((-cos(se1.data)*a3*px)+(-sin(se1.data)*a3*py)+(csc(se3.data)*a2*pz)+(cot(se3.data)*a3*pz)),((cos(se1.data)*csc(se3.data)*a2*px)+(cos(se1.data)*cot(se3.data)*a3*px)+(csc(se3.data)*sin(se1.data)*a2*py)+(cot(se3.data)*sin(se1.data)*a3*py)+(a3*pz)))+(PI/2);
  se4.data = asin(nz)-se2.data-se3.data;
    
	//printf("%.2f  %.2f  %.2f  %.2f  %.2f  %.2f\n",se1,se2,se3,se4,se5,se6 );
  //px = px +0.001;
  if(((px*px)+(py*py)+(pz*pz))>((a2+a3)*(a2+a3))||(((px*px)+(py*py)+(pz*pz)))<((a2-a3)*(a2-a3)))
  {
    printf("Out of Range\n");
  }
  else
  {
	

	joint.header.stamp = ros::Time::now();
    joint.name.resize(4);
    joint.position.resize(4);
    joint.name.push_back("right_upper_arm_joint");
    joint.name.push_back("right_elbow_joint");
    joint.name.push_back("right_wrist_joint");
    joint.name.push_back("right_wrist_joint2");
    

    joint.position.push_back(se1.data);
    joint.position.push_back(se2.data);
    joint.position.push_back(se3.data);
    joint.position.push_back(se4.data);



    chatter_pub.publish(joint);
    se1.data = -1*se1.data;
    se2.data = (-1*se2.data)+0.1;
    se3.data = se3.data-0.1;
    se4.data = -1*se4.data;
  }
    // head_joint_pos0.data = 0.0;
    // head_joint_pos1.data = 0.0;
    // mani_pub1.publish(1.57);
    // mani_pub3.publish(-1.7);
    // mani_pub2.publish(0.7);
    // mani_pub4.publish(0.38);
    // ros::Duration(5).sleep(); 
    // mani_pub1.publish(se1);
    // mani_pub3.publish(se3);
    // mani_pub2.publish(se2);
    // mani_pub4.publish(se4);
    // head0_pub.publish(head_joint_pos0);
    // head1_pub.publish(head_joint_pos1);


  //   switch(state)
  //   {
  //     case 0 : {
  //                 if(check == 0){

  //                       head0_pub.publish(0.0);
  //                       head1_pub.publish(0.0);
  //                       ros::Duration(0.5).sleep(); 
  //                       // if(check_pos_arm() == 0 || check_pos_head() == 0)
  //                       check = 1;
  //                 }
  //                 if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 1){
  //                       ros::Duration(5).sleep(); 
  //                       state = 1;
  //                       check = 2;
  //                       break;
  //                 }      
  //     }
  //     case 1 : {
  //                 if(check == 2){
  //                       mani_pub1.publish(1.57);
  //                       mani_pub3.publish(-2.07);
  //                       mani_pub2.publish(0.7);
  //                       mani_pub4.publish(0.4);
  //                       mani_pub5.publish(0.4);
  //                       head0_pub.publish(-1.57);
  //                       head1_pub.publish(-0.5);
  //                       ros::Duration(0.5).sleep(); 
  //                       // if(check_pos_arm() == 0 || check_pos_head() == 0)
  //                       check = 3;
  //                 }
  //                 if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 3){
  //                       ros::Duration(5).sleep(); 
  //                       state = 2;
  //                       check = 4;
  //                       break;
  //                 }
  //     }
  //     case 2 : {
  //                 if(check == 4){
  //                       mani_pub1.publish(se1);
  //                       mani_pub3.publish(se3);
  //                       mani_pub2.publish(se2);
  //                       mani_pub4.publish(se4);
  //                       ros::Duration(0.5).sleep();
  //                       // if(check_pos_arm() == 0 || check_pos_head() == 0)
  //                       check = 5;
  //                 }
  //                 if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 5){
  //                       ros::Duration(5).sleep(); 
  //                       state = 3;
  //                       check = 6;
  //                       break;
  //                 }                                   
  //     }
  //     case 3 : {
  //                 if(check == 6){

  //                       mani_pub5.publish(-0.6);
  //                       ros::Duration(0.5).sleep(); 
  //                       //if(check_pos_gripper()== 0)
  //                       check = 7;
  //                 }
  //                 if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 7){ 
  //                       ros::Duration(7).sleep(); 
  //                       state = 4;
  //                       check = 8;
  //                       break;
  //                 }                                   
  //     }
  //     case 4 : {
  //                 if(check == 8){

  //                       mani_pub1.publish(1.57);
  //                       mani_pub3.publish(-2.07);
  //                       mani_pub2.publish(0.7);
  //                       mani_pub4.publish(0.4);
  //                       ros::Duration(0.5).sleep(); 
  //                       // if(check_pos_arm() == 0 || check_pos_head() == 0)
  //                       check = 9;
  //                 }
  //                 if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 9){ 
  //                       ros::Duration(5).sleep(); 
  //                       state = 5;
  //                       check = 10;
  //                       break;
  //                 }                                   
  //     }      
  //     case 5 : {
  //                 if(check == 10){
  //                       mani_pub1.publish(0.0);
  //                       mani_pub3.publish(-1.6);
  //                       mani_pub2.publish(-1.2);
  //                       mani_pub4.publish(1.0);
  //                       head0_pub.publish(0.0);
  //                       head1_pub.publish(0.0);
  //                       ros::Duration(0.5).sleep(); 
  //                       // if(check_pos_arm() == 0 || check_pos_head() == 0)
  //                       check = 11;
  //                 }
  //                 if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 11){ 
  //                       ros::Duration(5).sleep(); 
  //                       state = 6;
  //                       check = 12;
  //                       break;
  //                 }                                   
  //     }
  //     case 6 : {
  //                 break;
  //     }    
  //   }

  // }

    // printf("%.2lf  %.2lf  %.2lf  %.2lf\n",se1.data,se2.data,se3.data,se4.data);
    printf("%d  %d  %d  %d\n",check_pos_arm(),check_pos_head(),check,state);
  	ros::spinOnce();

    loop_rate.sleep();
   
  }


  return 0;
}
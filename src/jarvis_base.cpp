#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <nav_msgs/Odometry.h>
#include <math.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>
#include <sensor_msgs/Imu.h>

ros::Time current_time,current_time2, last_time,last_time2;


double x;
double y;
double th;

double dt;
double delta_x;
double delta_y;
double delta_th;

double vx;
double vy;
double vth;

geometry_msgs::Quaternion imu_data;
double yaw_angle;
double yaw_angle_0;

double vel_angle;
bool init_yaw = true;

void feedbackCallback(const geometry_msgs::Twist& velo)
{
  //th = yaw_angle;
  current_time = ros::Time::now();
  dt = (current_time - last_time).toSec();
  vx = velo.linear.x;
  vy = velo.linear.y;
  vth = velo.angular.z;
  delta_x = (vx * cos(th) - vy * sin(th)) * dt;
  delta_y = (vx * sin(th) + vy * cos(th)) * dt;
  delta_th = vth * dt;
      x += delta_x;
    y += delta_y;
    th += delta_th;
  last_time = current_time;
}


// void imu_feedbackCallback(const sensor_msgs::Imu& imu_msg)
// {
//   imu_data.x = imu_msg.orientation.x;
//   imu_data.y = imu_msg.orientation.y;
//   imu_data.z = imu_msg.orientation.z;
//   imu_data.w = imu_msg.orientation.w;
//   vel_angle = imu_msg.angular_velocity.z;
    
//   yaw_angle = tf::getYaw(imu_msg.orientation);
//    if(init_yaw)
//     {yaw_angle_0 = yaw_angle;
//       init_yaw = false;}\
//     else
//     {
//       yaw_angle = yaw_angle - yaw_angle_0;
//     }
// }

int main(int argc, char** argv){
  
  ros::init(argc, argv, "jarvis_base");
  ros::NodeHandle n;
  ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>("odom", 50);
  ros::Subscriber sub = n.subscribe("cmd_feedback", 1000, feedbackCallback);
  //ros::Subscriber sub_imu = n.subscribe("/mavros/imu/data", 1000, imu_feedbackCallback);
  tf::TransformBroadcaster odom_broadcaster;

  
  current_time2 = ros::Time::now();
  last_time2 = ros::Time::now();

  ros::Rate r(23.0);
  while(n.ok()){

    ros::spinOnce();               // check for incoming messages
    current_time2 = ros::Time::now();


    
    // printf("%.2f   %.2f    %.2f\n",vx,vy,vth);
    //since all odometry is 6DOF we'll need a quaternion created from yaw
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);
     //geometry_msgs::Quaternion odom_quat = imu_data;
    //first, we'll publish the transform over tf
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = current_time2;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "odom_combined";

    odom_trans.transform.translation.x = x;
    odom_trans.transform.translation.y = y;
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation = odom_quat;
    //odom_trans.transform.rotation = imu_data;
    //send the transform
    odom_broadcaster.sendTransform(odom_trans);

    //next, we'll publish the odometry message over ROS
    nav_msgs::Odometry odom;
    odom.header.stamp = current_time2;
    odom.header.frame_id = "odom";

    //set the position
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;
    //odom.pose.pose.orientation = imu_data;
    //set the velocity
    odom.child_frame_id = "odom_combined";
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.angular.z = vth;

    //publish the message
    odom_pub.publish(odom);

    last_time2 = current_time2;

    ros::spinOnce();
    r.sleep();
  }
  return 0;
}

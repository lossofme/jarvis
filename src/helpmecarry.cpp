#include <ros/ros.h>
#include <math.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Pose.h>
#include "sensor_msgs/JointState.h"
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Twist.h>
#include "std_msgs/Float32.h"
#include <std_msgs/Float64.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/Int64.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Char.h>
#include <xy_to_xyz_msgs/BoundingBoxes_XYZ.h>
#include <xy_to_xyz_msgs/BoundingBox_XYZ.h>
#include <speech_jv/target.h>
#include <string>
#include <iostream>
#include <sound_play/sound_play.h>
#include <hark_msgs/HarkSource.h>
#include <hark_msgs/HarkSourceVal.h>
#include <std_msgs/Bool.h>
#include <string.h>
#define JOY_BUTTON_X 0
#define JOY_BUTTON_Y 3
#define JOY_BUTTON_A 1
#define JOY_BUTTON_B 2
#define JOY_BUTTON_L1 4
#define JOY_BUTTON_R1 5
#define JOY_BUTTON_R2 7
#define JOY_AXES_UPDOWN1 7
#define JOY_AXES_UPDOWN2 4
#define JOY_AXES_SLIP 6
#define JOY_AXES_LEFTRIGHT 0
#define JOY_AXES_TILTHEAD_X 2
#define JOY_AXES_TILTHEAD_Y 3
#define PI 3.1415926535897932384626433
#define ACCEPT_ERROR 0.01
#define ACCEPT_ERROR2 0.04

ros::Publisher chatter_pub,mani_pub1,mani_pub2,mani_pub3,mani_pub4,mani_pub5,head0_pub,head1_pub,navi_pub,simhead0_pub,simhead1_pub;
ros::Subscriber feedback_arm0,feedback_arm1,feedback_arm2,feedback_arm3,feedback_arm4,feedback_head0,feedback_head1,sub,sub2,sub3,sub_speech_,hark_sub;

ros::Publisher pub_save_image,cmd_vel_pub;

geometry_msgs::Transform point;
geometry_msgs::Pose nav_pose;
sensor_msgs::JointState joint;

std_msgs::Float64 se1;
std_msgs::Float64 se2;
std_msgs::Float64 se3;
std_msgs::Float64 se4;
std_msgs::Float64 se5;
std_msgs::Float64 head_joint_pos0;
std_msgs::Float64 head_joint_pos1;
speech_jv::target text;

double arm0,arm1,arm2,arm3,arm4,head0,head1;
double px,py,pz,sea,seo,sen,nx,ny,nz,ox,oy,oz,ax,ay,az,a2,a3;
int JOY_BUTTON[11];
float JOY_AXES[8];
int state = 1;
int check = 2;
int m_state = 0;
int m_check = 0;
int check_inverse = 0;
int finish_navi = 0;
int state_navi = 0;
int state_hark = 0;
xy_to_xyz_msgs::BoundingBoxes_XYZ img;
hark_msgs::HarkSource hks;
double radius[10] , angle[10] ,send_ang,rec_ang;
double send_ang0 = 1.57;
int count = 0;
bool sound_true=0;

double follow_x=0.0;
double follow_y=0.0;
double follow_z=0.0;
geometry_msgs::Twist tt;
double csc(double x)
{
  return 1/sin(x);
}
double cot(double x)
{
  return 1/tan(x);
}
void NavCallback(const std_msgs::Int64& msg)
{
  finish_navi = msg.data;
}
void arm0Callback(const dynamixel_msgs::JointState& a0)
{
  arm0 = abs(a0.error);
}
void arm1Callback(const dynamixel_msgs::JointState& a1)
{
  arm1 = abs(a1.error);
}
void arm2Callback(const dynamixel_msgs::JointState& a2)
{
  arm2 = abs(a2.error);
}
void arm3Callback(const dynamixel_msgs::JointState& a3)
{
  arm3 = abs(a3.error);
}
void arm4Callback(const dynamixel_msgs::JointState& a4)
{
  arm4 = abs(a4.error);
}
void head0Callback(const dynamixel_msgs::JointState& h0)
{
  head0 = abs(h0.error);
}
void head1Callback(const dynamixel_msgs::JointState& h1)
{
  head1 = abs(h1.error);
}
// void ImageCallback(const xy_to_xyz_msgs::BoundingBoxes_XYZ::ConstPtr& set)
// {
//   try
//   {

//     img=*set;

//     if (check_inverse != 1)
//       for(uint8_t i=0; i< img.boundingBoxes_xyz.size(); i++)
//       {
//        if(img.boundingBoxes_xyz[i].Class == "bottle" && img.boundingBoxes_xyz[i].probability > 0.55 )
//         {
//           point.translation.x = img.boundingBoxes_xyz[i].x;
//           point.translation.y = img.boundingBoxes_xyz[i].y;
//           point.translation.z = img.boundingBoxes_xyz[i].z;
//           ROS_INFO("Found it");
//            //break;
//         }
//       }
//       ROS_INFO("asd");
//   }
//   catch(int x)
//   {
//     ROS_INFO("CB ERROR");
//     // continue;
//   }
// }






// funtion
void ImageCallback(const xy_to_xyz_msgs::BoundingBoxes_XYZ::ConstPtr& set)
{
  
  try
  {

    img=*set;
    follow_x =0;
    follow_y=0;
    follow_z=0;
    if (check_inverse != 1)
      for(uint8_t i=0; i< img.boundingBoxes_xyz.size(); i++)
      {
       if(img.boundingBoxes_xyz[i].Class == "person" && img.boundingBoxes_xyz[i].probability > 0.50 &&img.boundingBoxes_xyz[i].z<2.0)
        {
        //   point.translation.x = img.boundingBoxes_xyz[i].x;
        //   point.translation.y = img.boundingBoxes_xyz[i].y;
        //   point.translation.z = img.boundingBoxes_xyz[i].z;
        //   ROS_INFO("Found it");
           //break;''
        follow_x=img.boundingBoxes_xyz[i].x;
        follow_y=img.boundingBoxes_xyz[i].y;
        follow_z=img.boundingBoxes_xyz[i].z;
        // break;
        }
      }
      
  }
  catch(int x)
  {
    ROS_INFO("CB ERROR");
    // continue;
  }
}

//main









void harkCallback(const hark_msgs::HarkSource::ConstPtr& msg)
{
  try
  {
    hks = *msg;
    
    for(uint8_t i=0; i< hks.src.size(); i++)
      {
        //radius[i] = sqrt(pow(hks.src[i].x,2)+pow(hks.src[i].y,2));
        if(hks.src[i].power > 27)
        {
          angle[i] = atan2(hks.src[i].x,hks.src[i].y)*180/PI;
          ROS_INFO("angle[%d] : %.2lf",i,angle[i]);
          state_hark = 1;
          //m_state = 1;
        }
        else if(hks.src[i].power < 27)
        {
          angle[i] = 90;
          ROS_INFO("angle[%d] : %.2lf",i,angle[i]);
          state_hark = 0;
        }
      }
  }
  catch(int x)
  {
    ROS_INFO("HK ERROR");
  }
}
int check_pos_arm()
{
  if(arm0 < ACCEPT_ERROR && arm1 < ACCEPT_ERROR && arm2 < ACCEPT_ERROR && arm3 < ACCEPT_ERROR)
    return 1;
  else
    return 0;
}
int check_pos_head()
{
  if(head0 < ACCEPT_ERROR && head1 < ACCEPT_ERROR)
    return 1;
  else
    return 0;
}
int check_pos_gripper()
{
  if(arm4 < ACCEPT_ERROR2)
    return 1;
  else
    return 0;
}
void JoyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
  for (int i=0; i<11 ;i++)
    JOY_BUTTON[i]=msg->buttons[i];
  for (int j=0; j<8 ;j++)
    JOY_AXES[j]=msg->axes[j];
}
void inverse_kinematic(geometry_msgs::Transform pos,std_msgs::Float64 & s1,std_msgs::Float64 & s2,std_msgs::Float64 & s3,std_msgs::Float64 & s4)
{

  ROS_INFO("inverse");
  // float a=0.04;
  // float b=0.17;
  // float c=0.3;
  
  float a=-0.04;
  float b=-0.3;
  float c=-0.17;
  float d=pos.translation.z;
  float e=pos.translation.y;
  float f=-pos.translation.x;
  float s=-PI/2;
  float bb=0.8;
  // px = a+ e*cos(b)+g*cos(s)*sin(b)-f*sin(s)  
  // py = b+f*cos(s)+e*cos(b)*sin(s)+g*sin(b)*sin(s)
  // pz = c+g*cos(b)-e*sin(s)
  // sea = s;
  // seo = b;
  // sen = PI/2;

  // px = pos.translation.x;  
  // py = -0.3;
  // pz = 0.3;
  // // sea = s;
  // seo = b;
  // sen = PI/2;

if(pos.translation.x > 0.1)
{
  px = -pos.translation.x-0.05;   
  py = -pos.translation.z*0.45;
  pz = pos.translation.y*0.85;
}
else if(pos.translation.x < 0.1)
{
  px = -pos.translation.x-0.05;   
  py = -pos.translation.z*0.45;
  pz = pos.translation.y*0.75;
}
else
{
  px = -pos.translation.x-0.05;   
  py = -pos.translation.z*0.3;
  pz = pos.translation.y*0.85;
}



  // px = pos.translation.x;   
  // py = pos.translation.y+0.2;
  // pz = pos.translation.z-0.3;
  sea = 0;
  seo = PI/2;
  sen = 0;
  



  // sea = pos.rotation.x;
  // seo = pos.rotation.y;
  // sen = pos.rotation.z;

  a2 = 0.175;
  a3 = 0.1702;
  nx = cos(sea)*cos(seo);
  ny = cos(seo)*sin(sea);
  nz = -sin(seo);
  ox = (-cos(sen)*sin(sea))+(cos(sea)*sin(sen)*sin(seo));
  oy = (cos(sea)*cos(sen))+(sin(sea)*sin(sen)*sin(seo));
  oz = cos(seo)*sin(sen);
  ax = (sin(sea)*sin(sen))+(cos(sea)*cos(sen)*sin(seo));
  ay = (-cos(sea)*sin(sen))+(cos(sen)*sin(sea)*sin(seo));
  az = cos(sen)*cos(seo);

  s1.data = atan2((py/(sqrt(pow(px,2)+pow(py,2)))),(px/(sqrt(pow(px,2)+pow(py,2)))));
  s3.data = -acos((-pow(a2,2)-pow(a3,2)+pow(px,2)+pow(py,2)+pow(pz,2))/(2*a2*a3));
  s2.data = atan2(((-cos(s1.data)*a3*px)+(-sin(s1.data)*a3*py)+(csc(s3.data)*a2*pz)+(cot(s3.data)*a3*pz)),((cos(s1.data)*csc(s3.data)*a2*px)+(cos(s1.data)*cot(s3.data)*a3*px)+(csc(s3.data)*sin(s1.data)*a2*py)+(cot(s3.data)*sin(s1.data)*a3*py)+(a3*pz)))+(PI/2);
  s4.data = asin(nz)-s2.data-s3.data;
    
  //printf("%.2f  %.2f  %.2f  %.2f  %.2f  %.2f\n",se1,se2,se3,se4,se5,se6 );
  //px = px +0.001;
  if(((px*px)+(py*py)+(pz*pz))>((a2+a3+0.2)*(a2+a3+0.2))||(((px*px)+(py*py)+(pz*pz)))<((a2-a3)*(a2-a3)))
  {
    printf("Out of Range\n");
    printf("%.2lf  %.2lf  %.2lf\n",px,py,pz);
    check_inverse = 10;
  }
  else
  {
  

  joint.header.stamp = ros::Time::now();
    joint.name.resize(4);
    joint.position.resize(4);
    joint.name.push_back("right_upper_arm_joint");
    joint.name.push_back("right_elbow_joint");
    joint.name.push_back("right_wrist_joint");
    joint.name.push_back("right_wrist_joint2");
    

    joint.position.push_back(s1.data);
    joint.position.push_back(s2.data);
    joint.position.push_back(s3.data);
    joint.position.push_back(s4.data);


    printf("%.2lf  %.2lf  %.2lf\n",px,py,pz);
    chatter_pub.publish(joint);
    s1.data = -1*s1.data;
    s2.data = (-1*s2.data)+0.1;
    s3.data = s3.data-0.1;
    s4.data = -1*s4.data;
    check_inverse = 1;
  }
}
void head_scan(std_msgs::Float64 & x,std_msgs::Float64 & y)
{
  x.data = -1.57;
  y.data = 0.5;
  head0_pub.publish(x);
  head1_pub.publish(y);
  // if(check_pos_head() ==1)
  // {
  //   simhead0_pub.publish(x);
  //   simhead1_pub.publish(y); 
  // } 
}
void head_localization(double x)
{

  head0_pub.publish(x);

  // if(check_pos_head() ==1)
  // {
  //   simhead0_pub.publish(x);
  //   simhead1_pub.publish(y); 
  // } 
}
void prepare_arm(std_msgs::Float64 & s1,std_msgs::Float64 & s2,std_msgs::Float64 & s3,std_msgs::Float64 & s4)
{
  s1.data = 0.0;
  s2.data = -1.2;
  s3.data = -1.6;
  s4.data = 1.0;
  mani_pub1.publish(0.0);
  mani_pub3.publish(-1.6);
  mani_pub2.publish(-1.2);
  mani_pub4.publish(1.0);
}
void prepare_head(std_msgs::Float64 & x,std_msgs::Float64 & y)
{
  x.data = 0;
  y.data = 0;
  head0_pub.publish(x);
  head1_pub.publish(y);
  // if(check_pos_head() ==1)
  // {
  //   simhead0_pub.publish(x);
  //   simhead1_pub.publish(y); 
  // } 
}
void manipulation(std_msgs::Float64 & s1,std_msgs::Float64 & s2,std_msgs::Float64 & s3,std_msgs::Float64 & s4)
{
  state = 1;
  check = 2;
  while(state < 6)
  {
    printf("mani : %d  %d\n",state,check );
    switch(state)
    {
      case 1 : {
                  if(check == 2){
                        mani_pub3.publish(-2.07);
                        mani_pub2.publish(0.7);
                        //mani_pub1.publish(1.0);
                        mani_pub4.publish(0.4);
                        mani_pub5.publish(0.4);
                        ros::Duration(7).sleep(); 
                        check = 20;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 20){
                        mani_pub1.publish(s1);
                        ros::Duration(5).sleep(); 
                        mani_pub4.publish(-1.0);
                        
                        check = 3;
                  }                  
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 3){
                        ros::Duration(5).sleep(); 
                        state = 2;
                        check = 4;
                        break;
                  }
      }
      case 2 : {
                  if(check == 4){
                        ROS_INFO("%.2lf  %.2lf  %.2lf  %.2lf +++++",s1.data,s2.data,s3.data,s4.data);
                        mani_pub1.publish(s1);
                        mani_pub3.publish(s3);
                        mani_pub2.publish(s2);
                        mani_pub4.publish(s4);
                        ros::Duration(0.5).sleep(); 
                        check = 5;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 5){
                        ros::Duration(5).sleep(); 
                        state = 3;
                        check = 6;
                        break;
                  }                                   
      }
      case 3 : {
                  if(check == 6){

                        mani_pub5.publish(-0.6);
                        ros::Duration(0.5).sleep(); 
                        //if(check_pos_gripper()== 0)
                        check = 7;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 7){ 
                        ros::Duration(5).sleep(); 
                        state = 4;
                        check = 8;
                        break;
                  }                                   
      }
      case 4 : {
                  if(check == 8){

                        mani_pub1.publish(1.57);
                        mani_pub3.publish(-2.07);
                        mani_pub2.publish(0.7);
                        mani_pub4.publish(0.4);
                        ros::Duration(0.5).sleep(); 
                        check = 9;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 9){ 
                        ros::Duration(5).sleep(); 
                        state = 5;
                        check = 10;
                        break;
                  }                                   
      }      
      case 5 : {
                  if(check == 10){
                        prepare_arm(se1,se2,se3,se4);
                        prepare_head(head_joint_pos0,head_joint_pos1);
                        ros::Duration(0.5).sleep(); 
                        check = 11;
                  }
                  if(check_pos_arm() == 1 && check_pos_head() == 1 && check == 11){ 
                        ros::Duration(5).sleep(); 
                        state = 6;
                        check = 12;
                        break;
                  }                                   
      }
      case 6 : { 
           
                  break;
      }    
    }
        ros::spinOnce();


  }
}
void SpeechCallback(const speech_jv::target::ConstPtr &msg)
{
 text = *msg;
 ROS_INFO("I heard a target : [%d] [%s] [%s] [%s]", text.q ,text.place.c_str(),text.object.c_str(),text.human.c_str()); 
 sound_true=1;
}
void sleepok(int t, ros::NodeHandle &n)
{
  if (n.ok())
      sleep(t);
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "helpmecarry");

 
  ros::NodeHandle n;
  sound_play::SoundClient sc;

  sub = n.subscribe("/joy", 1, &JoyCallback);
  sub2 = n.subscribe("/navi_finish", 1000, &NavCallback);
  sub3 = n.subscribe("/pub_to_main/object_detection_list", 1000, &ImageCallback);
  navi_pub  = n.advertise<geometry_msgs::Pose>("/nav_goal",1000);
  chatter_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1000);
  mani_pub1  = n.advertise<std_msgs::Float64>("arm0_controller/command",1000);  
  mani_pub2  = n.advertise<std_msgs::Float64>("arm1_controller/command",1000);
  mani_pub3  = n.advertise<std_msgs::Float64>("arm2_controller/command",1000);  
  mani_pub4  = n.advertise<std_msgs::Float64>("arm3_controller/command",1000);
  mani_pub5  = n.advertise<std_msgs::Float64>("arm4_controller/command",1000);
  head0_pub = n.advertise<std_msgs::Float64>("head0_controller/command",1000);
  head1_pub = n.advertise<std_msgs::Float64>("head1_controller/command",1000);
  simhead0_pub = n.advertise<std_msgs::Float64>("head0",1000);
  simhead1_pub = n.advertise<std_msgs::Float64>("head1",1000);

  cmd_vel_pub = n.advertise<geometry_msgs::Twist>("cmd_vel",100);

  feedback_arm0 = n.subscribe("arm0_controller/state", 1000, arm0Callback);
  feedback_arm1 = n.subscribe("arm1_controller/state", 1000, arm1Callback);
  feedback_arm2 = n.subscribe("arm2_controller/state", 1000, arm2Callback);
  feedback_arm3 = n.subscribe("arm3_controller/state", 1000, arm3Callback);
  feedback_arm4 = n.subscribe("arm4_controller/state", 1000, arm4Callback);
  feedback_head0 = n.subscribe("head0_controller/state", 1000, head0Callback);
  feedback_head1 = n.subscribe("head1_controller/state", 1000, head1Callback);
  sub_speech_ = n.subscribe("/Target", 1,SpeechCallback);
  hark_sub = n.subscribe("/hark_source",1000, harkCallback);
  pub_save_image  = n.advertise<std_msgs::Bool>("/sub_from_main_save",20);  
  ros::Rate loop_rate(10);


  while (ros::ok())
  {
    ROS_INFO("%.2lf  %.2lf  %.2lf",follow_x,follow_y,follow_z);
    switch(m_state)
    {
    case 0 :
    {
      prepare_arm(se1,se2,se3,se4);
      prepare_head(head_joint_pos0,head_joint_pos1);
      ros::Duration(2.0).sleep(); 
      if("ok" == text.place)
      {
              m_state = 1;
              break;
      }

      
    }
    case 1: //sound
    {

      if(sound_true)
      {
                    sc.say("OK I wake up");
      sleepok(5, n);
        m_state++;
      }
      break;
    }

    case 2:
    {
      if(text.place=="yes")
      {
        // system("x-terminal-emulator -e roslaunch darknet_ros darknet_ros.launch");
        ros::Duration(10).sleep();
        m_state++;
       }

      break;
    }

    case 3:
    {
      sc.say("OK I will follow you");
      sleepok(5, n); 
      sc.say("go go go");
      sleepok(2, n); 
      m_state++;
      break;
    }
    case 4:
    {
      if(follow_z>0.8&follow_z<2.0)
      {
        if(follow_x>0.05)// l
        {
          tt.linear.x=0.8;
          tt.linear.y=0;
          tt.angular.z=-0.8; //-0.3
          cmd_vel_pub.publish(tt);
        }
        else if(follow_x<-0.05)// l
        {
          tt.linear.x=0.8;
          tt.linear.y=0;
          tt.angular.z=0.8; //-0.3
          cmd_vel_pub.publish(tt);
        }
        else if(follow_x>=-0.05&&follow_x<=0.05)
        {
          tt.linear.x=0.8;
          tt.linear.y=0.0;
          tt.angular.z=0.0; //-0.3
          cmd_vel_pub.publish(tt);        
        }
        // else
        // {
        //   tt.linear.x=0;
        //   tt.linear.y=0;
        //   tt.angular.z=0; //-0.3
        //   cmd_vel_pub.publish(tt);
        // }
      }
      else if(follow_z>0.1&&follow_z<0.8)
      {
          tt.linear.x=-0.48;      
          tt.linear.y=0.0;
          tt.angular.z=0.0; //-0.3
          cmd_vel_pub.publish(tt); 

      }
      else 
      {
        sc.say("slow down");
        tt.linear.x=0;
         tt.linear.y=0;
        tt.angular.z=0;
        cmd_vel_pub.publish(tt);

      }

        tt.linear.x=0;
         tt.linear.y=0;
        tt.angular.z=0;




      // if(follow_z>0.8)
      // {
      // sc.say("slow down");
      // // sleepok(0., n);
      // }
      if (text.place=="stop")
      {
        sc.say("jarvis stop");
        // sleepok(5, n);
        m_state++;
      }
      break;
    }
  }


      
        

    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}

#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <std_msgs/Int64.h>
#include <math.h>

#define PI 3.1415926535897932384626433

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

move_base_msgs::MoveBaseGoal goal;
double yaw_angle = 0;
double yaw_angle0 = 0;
double x;
double y;
std_msgs::Int64 finish;
ros::Publisher status_navi;
double sum_x,sum_y,sum_yaw;
// geometry_msgs::Pose cur;
// void CurCallback(const geometry_msgs::PoseStamped& cur_msg)
// {
//   cur.position.x = cur_msg.pose.position.x;
//   cur.position.y = cur_msg.pose.position.y;
//   cur.orientation = cur_msg.pose.orientation;
// }
void NavCallback(const geometry_msgs::Pose& msg)
{
  MoveBaseClient ac("move_base", true);
  while (!ac.waitForServer(ros::Duration(5.0)))
  {
    ROS_INFO("Waiting for the move_base action server to come up");
  }
    yaw_angle = tf::getYaw(msg.orientation);
    goal.target_pose.header.frame_id = "base_link";
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose.position.x =  ((msg.position.x-x)*cos(yaw_angle0))+
                                        ((msg.position.y-y)*sin(yaw_angle0));

    goal.target_pose.pose.position.y =  -((msg.position.x-x)*sin(yaw_angle0))+
                                        ((msg.position.y-y)*cos(yaw_angle0));
                                        
    goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_angle-yaw_angle0);

    ROS_INFO("Sending goal");
    
    //ROS_INFO("%.2lf  %.2lf  %.2lf  %.2lf  %.2lf  %.2lf",goal.target_pose.pose.position.x,goal.target_pose.pose.position.y,x,y,msg.position.x,msg.position.y);
    ac.sendGoal(goal);

    ac.waitForResult();
    // ROS_INFO("Amin");
    if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
      ROS_INFO("FINISH NAVIGATION");
      // x = cur.position.x;
      // y = cur.position.y;
      // yaw_angle0 = tf::getYaw(cur.orientation);
      x = msg.position.x;
      y = msg.position.y;
      yaw_angle0 = yaw_angle;
      finish.data = 1;
      if(finish.data ==1)
      {
        status_navi.publish(finish);
        finish.data = 0;
        system("rosnode kill /cartographer_node");
        system("rosnode kill /cartographer_occupancy_grid_node"); 
        // system("rosnode kill /scan"); 
        ROS_INFO("%.2lf  %.2lf  %.2f",goal.target_pose.pose.position.x,goal.target_pose.pose.position.y,tf::createQuaternionMsgFromYaw(yaw_angle-yaw_angle0));      
      }
    }
    else
    {
      ROS_INFO("The base failed to move for some reason");
    }
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "navigation_goals_localization");
  ros::NodeHandle n;

  
  ros::Subscriber sub = n.subscribe("/nav_goal", 1000, &NavCallback);
  // ros::Subscriber sub1 = n.subscribe("/move_base_node/current_goal", 1000, &CurCallback);
  status_navi = n.advertise<std_msgs::Int64>("/navi_finish", 1000);
  // while(!ac.waitForServer(ros::Duration(5.0))){
  //   ROS_INFO("Waiting for the move_base action server to come up");
  // }
  ros::spin();

  return 0;
}

#include <ros/ros.h>
#include <math.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <sensor_msgs/Joy.h>
#include <dynamixel_msgs/JointState.h>
#include <unistd.h>

#define JOY_BUTTON_X 0
#define JOY_BUTTON_Y 3
#define JOY_BUTTON_A 1
#define JOY_BUTTON_B 2
#define JOY_BUTTON_L1 4
#define JOY_BUTTON_R1 5
#define JOY_BUTTON_R2 7
#define JOY_AXES_UPDOWN1 3
#define JOY_AXES_UPDOWN2 5
#define JOY_AXES_SLIP 6
#define JOY_AXES_LEFTRIGHT 0
#define JOY_AXES_TILTHEAD_X 2
#define JOY_AXES_TILTHEAD_Y 7

int JOY_BUTTON[11];
float JOY_AXES[8];


void sleepok(int t, ros::NodeHandle &n)
{
  if (n.ok())
      sleep(t);
}

void JoyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
	for (int i=0; i<11 ;i++)
		JOY_BUTTON[i]=msg->buttons[i];
	for (int j=0; j<11 ;j++)
		JOY_AXES[j]=msg->axes[j];
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "jarvis_joy");

 
  ros::NodeHandle n;
  //sound_play::SoundClient sc;

  double CURR_TIME ,EXPECT_TIME ,CURR_TIME2 ,EXPECT_TIME2;
  int state=0,cycle=0,state2=0;
  float CURR_POS=0 ,delay_time=1.5 ,CURR_POS2 ,GRIPPER=-0.6;


  ros::Subscriber sub = n.subscribe("/joy", 1000, &JoyCallback);
  /*ros::Publisher chatter_pub1 = n.advertise<std_msgs::Float64>("/arm1motor1/command", 1000);
  ros::Publisher chatter_pub2 = n.advertise<std_msgs::Float64>("/arm1motor2/command", 1000);
  ros::Publisher chatter_pub3 = n.advertise<std_msgs::Float64>("/arm1motor3/command", 1000);
  ros::Publisher chatter_pub4 = n.advertise<std_msgs::Float64>("/arm1motor4/command", 1000);
  ros::Publisher chatter_pub5 = n.advertise<std_msgs::Float64>("/arm1motor5/command", 1000);*/
  ros::Publisher chatter_pub6  = n.advertise<std_msgs::Float64>("wheel0_controller/command",1000);  
  ros::Publisher chatter_pub7  = n.advertise<std_msgs::Float64>("wheel1_controller/command",1000);
  ros::Publisher chatter_pub8  = n.advertise<std_msgs::Float64>("wheel2_controller/command",1000);  
  ros::Publisher chatter_pub9  = n.advertise<std_msgs::Float64>("wheel3_controller/command",1000);
  /*ros::Publisher chatter_pub10 = n.advertise<std_msgs::Float64>("/head1_controller/command", 1000);
  ros::Publisher chatter_pub15 = n.advertise<std_msgs::Float64>("/head0_controller/command", 1000);
  //for left arm
  ros::Publisher chatter_pub11 = n.advertise<std_msgs::Float64>("/arm2motor1/command", 1000);
  ros::Publisher chatter_pub12 = n.advertise<std_msgs::Float64>("/arm2motor2/command", 1000);
  ros::Publisher chatter_pub13 = n.advertise<std_msgs::Float64>("/arm2motor3/command", 1000);
  ros::Publisher chatter_pub14 = n.advertise<std_msgs::Float64>("/arm2motor4/command", 1000);*/


  ros::Rate loop_rate(30);
  std_msgs::Float64 pos1,pos2,pos3,pos4,pos5,pos10,pos11,pos12,pos13,pos14,pos15;
  std_msgs::Float64 omegaWheel1;
  std_msgs::Float64 omegaWheel2;
  std_msgs::Float64 omegaWheel3;
  std_msgs::Float64 omegaWheel4;
  float speed=2.6;
  while (ros::ok())
  {
   //printf("%.1f\n",speed);

   /*if(JOY_BUTTON[JOY_BUTTON_X] == 1)
   { //sawasdee
    
    pos1.data = -0.30;
    pos2.data = 0.38;
    pos3.data = -1.47;
    pos4.data = 0.57;
    pos5.data = -0.51;
    
    pos10.data = -0.5; //head joint
    
    pos11.data = 0.2;
    pos12.data = 0.38;
    pos13.data = -1.5;
    pos14.data = 1.1;


    chatter_pub1.publish(pos1);
    chatter_pub2.publish(pos2);
    chatter_pub3.publish(pos3);
    chatter_pub4.publish(pos4);
    chatter_pub5.publish(pos5);

    //left arm
    chatter_pub11.publish(pos11);
    chatter_pub12.publish(pos12);
    chatter_pub13.publish(pos13);
    chatter_pub14.publish(pos14);
    
    CURR_TIME =ros::Time::now().toSec();

    if(state == 0 ){
    	state =1;
    	EXPECT_TIME = CURR_TIME+4; //wait for 5 second 
    	CURR_POS = 0.0;
    	delay_time = 1.5;
    }

    if(CURR_TIME >= EXPECT_TIME){
    	printf("ARM Reach position start moving\n" );
    	state = 2;

    }else printf("current time : %.2f \texpected time : %.2f\n",CURR_TIME ,EXPECT_TIME );

    //ros::Duration(4).sleep();
    if(state == 2)
    	chatter_pub10.publish(pos10);
    }
else if(JOY_BUTTON[JOY_BUTTON_Y] == 1)
{
	//chern position
	pos1.data = 0.0;
    pos2.data = 0.0;
    pos3.data = 0.0;
    pos4.data = -0.7;
    pos5.data = GRIPPER; //free to ctrl gripper

    	pos11.data = 0.0;
    	pos12.data = 0.0;
    	pos13.data = 0.0;
    	pos14.data = 0.0;
    chatter_pub1.publish(pos1);
    chatter_pub2.publish(pos2);
    chatter_pub3.publish(pos3);
    chatter_pub4.publish(pos4);
    chatter_pub5.publish(pos5);

        	chatter_pub11.publish(pos11);
    	chatter_pub12.publish(pos12);
   	 	chatter_pub13.publish(pos13);
    	chatter_pub14.publish(pos14);
    sc.playWave("/home/dongyang/catkin_ws/src/audio_common/sound_play/sounds/Javis1.wav");
    sleepok(7, n);
    sc.playWave("/home/dongyang/catkin_ws/src/audio_common/sound_play/sounds/Javis2.wav");

    
}*/
if((JOY_AXES[JOY_AXES_UPDOWN1] == 1) || (JOY_AXES[JOY_AXES_UPDOWN2] == 1))
   {
    omegaWheel1.data = speed;
    omegaWheel2.data = speed;
    omegaWheel3.data = -speed;
    omegaWheel4.data = -speed;
    chatter_pub6.publish(omegaWheel1);
    chatter_pub7.publish(omegaWheel2);
    chatter_pub8.publish(omegaWheel3);
    chatter_pub9.publish(omegaWheel4);
   }
   else if((JOY_AXES[JOY_AXES_UPDOWN1] == -1) || (JOY_AXES[JOY_AXES_UPDOWN2] == -1))
   {
    omegaWheel1.data = -speed;
    omegaWheel2.data = -speed;
    omegaWheel3.data = speed;
    omegaWheel4.data = speed;
    chatter_pub6.publish(omegaWheel1);
    chatter_pub7.publish(omegaWheel2);
    chatter_pub8.publish(omegaWheel3);
    chatter_pub9.publish(omegaWheel4);
   }
else if(JOY_AXES[JOY_AXES_LEFTRIGHT] == 1)
   {
    omegaWheel1.data = -speed;
    omegaWheel2.data = -speed;
    omegaWheel3.data = -speed;
    omegaWheel4.data = -speed;
    chatter_pub6.publish(omegaWheel1);
    chatter_pub7.publish(omegaWheel2);
    chatter_pub8.publish(omegaWheel3);
    chatter_pub9.publish(omegaWheel4);
   }
else if(JOY_AXES[JOY_AXES_LEFTRIGHT] == -1)
   {
    omegaWheel1.data = speed;
    omegaWheel2.data = speed;
    omegaWheel3.data = speed;
    omegaWheel4.data = speed;
    chatter_pub6.publish(omegaWheel1);
    chatter_pub7.publish(omegaWheel2);
    chatter_pub8.publish(omegaWheel3);
    chatter_pub9.publish(omegaWheel4);
   }
   else if(JOY_AXES[JOY_AXES_SLIP] == -1)
   {
    omegaWheel1.data = speed;
    omegaWheel2.data = -speed;
    omegaWheel3.data = speed;
    omegaWheel4.data = -speed;
    chatter_pub6.publish(omegaWheel1);
    chatter_pub7.publish(omegaWheel2);
    chatter_pub8.publish(omegaWheel3);
    chatter_pub9.publish(omegaWheel4);
   }
   else if(JOY_AXES[JOY_AXES_SLIP] == 1)
   {
    omegaWheel1.data = -speed;
    omegaWheel2.data = speed;
    omegaWheel3.data = -speed;
    omegaWheel4.data = speed;
    chatter_pub6.publish(omegaWheel1);
    chatter_pub7.publish(omegaWheel2);
    chatter_pub8.publish(omegaWheel3);
    chatter_pub9.publish(omegaWheel4);
   }
  else if(JOY_BUTTON[JOY_BUTTON_L1] == 1)
  {
   if(speed > 0.4)
   {
    speed = speed-0.2;
   }
   else if(speed <= 0.4)
   {
    speed = speed;
   }
  }
  else if(JOY_BUTTON[JOY_BUTTON_R1] == 1)
  {
   if(speed < 4.0)
   {
    speed = speed+0.2;
   }
   else if(speed >= 0.2)
   {
    speed = speed;
   }
  }

  /*else if(JOY_BUTTON[JOY_BUTTON_A] == 1){
  	//bye bye position
  	if(state ==0 || state ==1){
  		pos1.data = 0.0;
    	pos2.data = 0.3;
    	pos3.data = -1.6;
    	pos4.data = 0.4;
    	pos5.data = -0.6;

    	//left arm
    	pos11.data = 0.0;
    	pos12.data = 0.3;
    	pos13.data = -1.6;
    	pos14.data = 1.1;


    	chatter_pub1.publish(pos1);
    	chatter_pub2.publish(pos2);
    	chatter_pub3.publish(pos3);
    	chatter_pub4.publish(pos4);
    	chatter_pub5.publish(pos5);
    	//left arm
    	chatter_pub11.publish(pos11);
    	chatter_pub12.publish(pos12);
   	 	chatter_pub13.publish(pos13);
    	chatter_pub14.publish(pos14);
    }

    CURR_TIME =ros::Time::now().toSec();
    
    

    if(state == 0 ){
    	state =1;
    	EXPECT_TIME = CURR_TIME+5.0; //wait for 5 second 
    	CURR_POS = 0.0;
    	CURR_POS2 = 0.0;
    	delay_time = 1.5;
    }

    if(CURR_TIME >= EXPECT_TIME){
    	printf("ARM Reach position start moving\n" );
    	state = 2;

    }else printf("current time : %.2f \texpected time : %.2f\n",CURR_TIME ,EXPECT_TIME );

    if(state == 2 ){
    	
    	CURR_TIME2 =ros::Time::now().toSec();
    	
    	if(state2==0){
    		
    		EXPECT_TIME2 = CURR_TIME2+delay_time; //wait for 3.2 second
    		if(cycle==0){
    			CURR_POS =  0.7;
    			CURR_POS2 = -0.5;
    			cycle = 1;
    		}
    		else {
    			CURR_POS = -0.2;
    			cycle = 0;
    			CURR_POS2 = 0.2;
    		}
    		state2=1;

    		delay_time = 1.9;
    	}

    	if(CURR_TIME2 >= EXPECT_TIME2)
    		state2=0;

    	printf("current pub position : %.1f \n",CURR_POS );
    	pos1.data = CURR_POS;
    	pos2.data = 0.3;
    	pos3.data = -1.6;
    	pos4.data = 0.4;
    	pos5.data = -0.6;

    	//left arm
    	pos11.data = CURR_POS2;
    	pos12.data = 0.3;
    	pos13.data = -1.6;
    	pos14.data = 1.1;

    	chatter_pub1.publish(pos1);
    	chatter_pub2.publish(pos2);
    	chatter_pub3.publish(pos3);
    	chatter_pub4.publish(pos4);
    	chatter_pub5.publish(pos5);
    	    	//left arm
    	chatter_pub11.publish(pos11);
    	chatter_pub12.publish(pos12);
   	 	chatter_pub13.publish(pos13);
    	chatter_pub14.publish(pos14);

    }


  }

else if(JOY_BUTTON[JOY_BUTTON_B] == 1)
   {
    //... position
    if(state ==0 || state ==1){
    pos1.data = 0.0;
    pos2.data = 0.707;
    pos3.data = -0.707;
    pos4.data = -0.5;
    pos5.data = GRIPPER;

       	pos11.data = 0.0;
    	pos12.data = 0.0;
    	pos13.data = 0.0;
    	pos14.data = 0.0;
    chatter_pub1.publish(pos1);
    chatter_pub2.publish(pos2);
    chatter_pub3.publish(pos3);
    chatter_pub4.publish(pos4);
    chatter_pub5.publish(pos5);

        	chatter_pub11.publish(pos11);
    	chatter_pub12.publish(pos12);
   	 	chatter_pub13.publish(pos13);
    	chatter_pub14.publish(pos14);
   }
   CURR_TIME =ros::Time::now().toSec();
    
    

    if(state == 0 ){
    	state =1;
    	EXPECT_TIME = CURR_TIME+5.0; //wait for 5 second 
    	CURR_POS = 0.7;
    	
    	delay_time = 1.5;
    }


    	if(CURR_TIME>= EXPECT_TIME)
    		{

    	printf("current pub position : %.1f \n",CURR_POS );
    	pos1.data = CURR_POS;
    	pos2.data = 0.3;
       pos2.data = 0.707;
    pos3.data = -0.707;
    pos4.data = -0.5;
    pos5.data = GRIPPER;

       	pos11.data = 0.0;
    	pos12.data = 0.0;
    	pos13.data = 0.0;
    	pos14.data = 0.0;

    	chatter_pub1.publish(pos1);
    	chatter_pub2.publish(pos2);
    	chatter_pub3.publish(pos3);
    	chatter_pub4.publish(pos4);
    	chatter_pub5.publish(pos5);
    	    	//left arm
    	chatter_pub11.publish(pos11);
    	chatter_pub12.publish(pos12);
   	 	chatter_pub13.publish(pos13);
    	chatter_pub14.publish(pos14);

    }

    }*/
else 
   { //default position
    /*state = 0;
    state2 =0;

    pos1.data = -0.01;
    pos2.data = -0.7;
    pos3.data = -1.4;
    pos4.data = 0.2;
    pos5.data = 0.0;
   
    pos10.data = -0.10;*/
    omegaWheel1.data = 0.0;
    omegaWheel2.data = 0.0;
    omegaWheel3.data = 0.0;
    omegaWheel4.data = 0.0;
    //left arm
    /*pos11.data = -0.01;
    pos12.data = -0.7;
    pos13.data = -1.4;
    pos14.data = 0.9;*/

    
    /*chatter_pub1.publish(pos1);
    chatter_pub2.publish(pos2);
    chatter_pub3.publish(pos3);
    chatter_pub4.publish(pos4);
    chatter_pub5.publish(pos5);
    chatter_pub10.publish(pos10);*/
    chatter_pub6.publish(omegaWheel1);
    chatter_pub7.publish(omegaWheel2);
    chatter_pub8.publish(omegaWheel3);
    chatter_pub9.publish(omegaWheel4);

    //left arm
    /*chatter_pub11.publish(pos11);
    chatter_pub12.publish(pos12);
    chatter_pub13.publish(pos13);
    chatter_pub14.publish(pos14);*/



    }
    

    /*if(JOY_AXES[JOY_AXES_TILTHEAD_X] > 0){
    	//tilt head x axis	
    	pos15.data = JOY_AXES[JOY_AXES_TILTHEAD_X]; //head x joint
    	chatter_pub15.publish(pos15);

    }else if(JOY_AXES[JOY_AXES_TILTHEAD_X] < 0){
    	//tilt head x axis
    	pos15.data = JOY_AXES[JOY_AXES_TILTHEAD_X]; //head x joint
    	chatter_pub15.publish(pos15);
    }else {
    	pos15.data = 0.0; //head x joint
    	chatter_pub15.publish(pos15);
    }

    if(JOY_AXES[JOY_AXES_TILTHEAD_Y] > 0){
    	//tilt head y axis	
    	pos10.data = JOY_AXES[JOY_AXES_TILTHEAD_Y]*-1; //head y joint
    	chatter_pub10.publish(pos10);
    }




    if(JOY_BUTTON[JOY_BUTTON_R2] == 1){
    	//force open gripper
    	GRIPPER = 0.5; //open
    	pos5.data = GRIPPER;
    	chatter_pub5.publish(pos5);

    }
   	else GRIPPER = -0.6; //closed*/

    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}

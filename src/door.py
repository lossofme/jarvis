#!/usr/bin/env python
# import roslib
#roslib.load_manifest('jarvis')
import sys
import rospy
import cv2
from std_msgs.msg import String
from geometry_msgs.msg import Pose2D
from std_msgs.msg import Bool
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError



import numpy as np

msg=Pose2D()

def nothing(x):
	return() 

class image_converter:


	def __init__(self):
		self.image_pub = rospy.Publisher('/pub_to_main/door',Bool,queue_size=1)

		self.bridge = CvBridge()
		self.depth_sub2 = rospy.Subscriber('/camera/depth/image_rect_raw',Image,self.callback_image_depth)
		print "init finish"

	def callback_image_depth(self,data):
		try:
			self.cv_depth = self.bridge.imgmsg_to_cv2(data, "16UC1")
			count_near=0
			count_far=0
			# print self.cv_depth[240,320]
			for x in xrange(160,480):
				for y in xrange(120,360):
					if self.cv_depth[x,y]<200:
						count_near=count_near+1
					else:
						count_far=count_far+1
			if count_far>count_near:
				self.image_pub.publish(1)
			else:
				self.image_pub.publish(0)
		except CvBridgeError as e:
			print(e)
	
		
def main(args):
	ic = image_converter()
	rospy.init_node('door', anonymous=True)
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
	cv2.destroyAllWindows()

if __name__ == '__main__':
		main(sys.argv)

#include <ros/ros.h>
#include <math.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Pose.h>


geometry_msgs::Pose nav_pose;

#define JOY_BUTTON_X 0
#define JOY_BUTTON_Y 3
#define JOY_BUTTON_A 1
#define JOY_BUTTON_B 2
#define JOY_BUTTON_L1 4
#define JOY_BUTTON_R1 5
#define JOY_BUTTON_R2 7
#define JOY_AXES_UPDOWN1 7
#define JOY_AXES_UPDOWN2 4
#define JOY_AXES_SLIP 6
#define JOY_AXES_LEFTRIGHT 0
#define JOY_AXES_TILTHEAD_X 2
#define JOY_AXES_TILTHEAD_Y 3

int JOY_BUTTON[11];
float JOY_AXES[8];

void JoyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
	for (int i=0; i<11 ;i++)
		JOY_BUTTON[i]=msg->buttons[i];
	for (int j=0; j<8 ;j++)
		JOY_AXES[j]=msg->axes[j];
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "main_navi");

 
  ros::NodeHandle n;



  ros::Subscriber sub = n.subscribe("/joy", 1, &JoyCallback);
  ros::Publisher navi_pub  = n.advertise<geometry_msgs::Pose>("/nav_goal",1000);


  ros::Rate loop_rate(10);


  while (ros::ok())
  {

  	if(JOY_BUTTON[JOY_BUTTON_A] == 1)
   {
   	ROS_INFO("11111111111111111111");
   	nav_pose.position.x = 1.0;
   	nav_pose.position.y = 0.0;
   	nav_pose.orientation.z = 0.0;
   	nav_pose.orientation.w = 1.0;
   	navi_pub.publish(nav_pose);
   }
    else if(JOY_BUTTON[JOY_BUTTON_X] == 1)
   {
   	ROS_INFO("22222222222222222222");
   	nav_pose.position.x = 2.0;
   	nav_pose.position.y = -1.0;
   	nav_pose.orientation.z = 0.0;
   	nav_pose.orientation.w = 1.0;
   	navi_pub.publish(nav_pose);
   }
    else if(JOY_BUTTON[JOY_BUTTON_B] == 1)
   {
   	ROS_INFO("33333333333333333333");
   	nav_pose.position.x = 0.0;
   	nav_pose.position.y = 0.0;
   	nav_pose.orientation.z = 0.0;
   	nav_pose.orientation.w = 1.0;
   	navi_pub.publish(nav_pose);
   }


    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
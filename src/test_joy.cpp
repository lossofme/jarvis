#include <ros/ros.h>
#include <math.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <sensor_msgs/Joy.h>
#include <dynamixel_msgs/JointState.h>
#include <unistd.h>

#define JOY_BUTTON_X 0
#define JOY_BUTTON_Y 3
#define JOY_BUTTON_A 1
#define JOY_BUTTON_B 2
#define JOY_BUTTON_L1 4
#define JOY_BUTTON_R1 5
#define JOY_BUTTON_R2 7
#define JOY_AXES_UPDOWN1 7
#define JOY_AXES_UPDOWN2 4
#define JOY_AXES_SLIP 6
#define JOY_AXES_LEFTRIGHT 0
#define JOY_AXES_TILTHEAD_X 2
#define JOY_AXES_TILTHEAD_Y 3

int JOY_BUTTON[11];
float JOY_AXES[5];

  
void sleepok(int t, ros::NodeHandle &n)
{
  if (n.ok())
      sleep(t);
}

void JoyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
	for (int i=0; i<11 ;i++)
		JOY_BUTTON[i]=msg->buttons[i];
	for (int j=0; j<5 ;j++)
		JOY_AXES[j]=msg->axes[j];
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "test_joy");

 
  ros::NodeHandle n;
  //sound_play::SoundClient sc;

  double CURR_TIME ,EXPECT_TIME ,CURR_TIME2 ,EXPECT_TIME2;
  int state=0,cycle=0,state2=0;
  float CURR_POS=0 ,delay_time=1.5 ,CURR_POS2 ,GRIPPER=-0.6;


  ros::Subscriber sub = n.subscribe("/joy", 1000, &JoyCallback);
  /*ros::Publisher chatter_pub1 = n.advertise<std_msgs::Float64>("/arm1motor1/command", 1000);
  ros::Publisher chatter_pub2 = n.advertise<std_msgs::Float64>("/arm1motor2/command", 1000);
  ros::Publisher chatter_pub3 = n.advertise<std_msgs::Float64>("/arm1motor3/command", 1000);
  ros::Publisher chatter_pub4 = n.advertise<std_msgs::Float64>("/arm1motor4/command", 1000);
  ros::Publisher chatter_pub5 = n.advertise<std_msgs::Float64>("/arm1motor5/command", 1000);*/
  ros::Publisher chatter_pub6  = n.advertise<std_msgs::Float64>("wheel0_controller/command",1000);  
  ros::Publisher chatter_pub7  = n.advertise<std_msgs::Float64>("wheel1_controller/command",1000);
  ros::Publisher chatter_pub8  = n.advertise<std_msgs::Float64>("wheel2_controller/command",1000);  
  ros::Publisher chatter_pub9  = n.advertise<std_msgs::Float64>("wheel3_controller/command",1000);
  /*ros::Publisher chatter_pub10 = n.advertise<std_msgs::Float64>("/head1_controller/command", 1000);
  ros::Publisher chatter_pub15 = n.advertise<std_msgs::Float64>("/head0_controller/command", 1000);
  //for left arm
  ros::Publisher chatter_pub11 = n.advertise<std_msgs::Float64>("/arm2motor1/command", 1000);
  ros::Publisher chatter_pub12 = n.advertise<std_msgs::Float64>("/arm2motor2/command", 1000);
  ros::Publisher chatter_pub13 = n.advertise<std_msgs::Float64>("/arm2motor3/command", 1000);
  ros::Publisher chatter_pub14 = n.advertise<std_msgs::Float64>("/arm2motor4/command", 1000);*/


  ros::Rate loop_rate(30);
  std_msgs::Float64 pos1,pos2,pos3,pos4,pos5,pos10,pos11,pos12,pos13,pos14,pos15;
  std_msgs::Float64 omegaWheel1;
  std_msgs::Float64 omegaWheel2;
  std_msgs::Float64 omegaWheel3;
  std_msgs::Float64 omegaWheel4;
  float speed=2.6;
  while (ros::ok())
  {
    if(JOY_BUTTON[JOY_BUTTON_A] == 1)
   {
      ROS_INFO("111111111");
      system("x-terminal-emulator -e roslaunch darknet_ros darknet_ros.launch");
   }
    else if(JOY_BUTTON[JOY_BUTTON_X] == 1)
   {
      system("rosnode kill /darknet_ros");
   }
    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}

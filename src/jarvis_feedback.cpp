#include "ros/ros.h"
#include "std_msgs/String.h"
#include <dynamixel_msgs/JointState.h>
#include <math.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>

#define PI 3.1415926535897932384626433
#define L1 0.23  
#define L2 0.137
#define R 0.076

double wheel1,wheel2,wheel3,wheel4;

void wheel0Callback(const dynamixel_msgs::JointState& msg0)
{
  wheel1 = msg0.velocity;
}
void wheel1Callback(const dynamixel_msgs::JointState& msg1)
{
  wheel3 = msg1.velocity;
}
void wheel2Callback(const dynamixel_msgs::JointState& msg2)
{
  wheel2 =-msg2.velocity;
}
void wheel3Callback(const dynamixel_msgs::JointState& msg3)
{
  wheel4 = -msg3.velocity;
}
int main(int argc, char **argv)
{
 
  ros::init(argc, argv, "jarvis_feedback");

  ros::NodeHandle n;
  

  ros::Publisher pub = n.advertise<geometry_msgs::Twist>("cmd_feedback", 50);
  ros::Subscriber sub0 = n.subscribe("wheel0_controller/state", 1000, wheel0Callback);
  ros::Subscriber sub1 = n.subscribe("wheel1_controller/state", 1000, wheel1Callback);
  ros::Subscriber sub2 = n.subscribe("wheel2_controller/state", 1000, wheel2Callback);
  ros::Subscriber sub3 = n.subscribe("wheel3_controller/state", 1000, wheel3Callback);
  ros::Rate loop_rate(100);
  geometry_msgs::Twist velo;

  while (ros::ok())
  {
    //printf("%.2f   %.2f    %.2f   %.2f\n",wheel1,wheel2,wheel3,wheel4);
    velo.linear.x = ((wheel1 + wheel2 + wheel3 + wheel4)/4)*R;
    velo.linear.y = ((- wheel1 + wheel2 + wheel3 - wheel4)/4)*R;
    velo.angular.z = ((- wheel1 + wheel2 - wheel3 + wheel4)/(4*(L1 + L2)))*R;
    pub.publish(velo);
    ros::spinOnce();

    loop_rate.sleep();
  }
  return 0;
}

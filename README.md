#### Dependencies

- [Robot Operating System (ROS)](http://wiki.ros.org) (middleware for robotics),
- [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) (linear algebra library)

		sudo apt-get install libeigen3-dev

- [Dynamixel](http://wiki.ros.org/dynamixel_controllers) (dynamixel)
- [PCL](http://pointclouds.org/) (Pointcloud Library)
- [Moveit](http://moveit.ros.org/robots/) (Moveit)
- [darknet_ros](https://github.com/leggedrobotics/darknet_ros) (Darknet_ros)
- [cartographer_ros](https://github.com/larics/cartographer_superbuild) (Cartographer)
- [slam_gmapping](https://github.com/ros-perception/slam_gmapping) (slam_gmapping) 
#### Jarvis build map with cartographer
	roscore
	
	sudo chmod a+rw /dev/ttyUSB0

	sudo chmod a+rw /dev/ttyUSB1

	sudo chmod a+rw /dev/ttyACM0
	
	roslaunch jarvis rviz_jarvis.launch 
	
	roslaunch jarvis carto_mapping_movebase.launch
	
	rosbag record -a
	
	roslaunch jarvis movebase_joy.launch
	
	roslaunch jarvis startall.launch
	
###### after slam

	roslaunch jarvis offline_carto.launch bag_filenames:= your bagfile.bag

#### Jarvis navigate with cartographer
	
	roscore
	
	sudo chmod a+rw /dev/ttyUSB0

	sudo chmod a+rw /dev/ttyUSB1

	sudo chmod a+rw /dev/ttyACM0
	
	roslaunch jarvis rviz_jarvis.launch
	
	roslaunch jarvis movebase.launch
	
	roslaunch jarvis carto_localization.launch map_filename:= your mapfile.bag.pbstream
	
	roslaunch jarvis startall.launch
	
#### Jarvis build map with gmapping
	roscore
	
	sudo chmod a+rw /dev/ttyUSB0

	sudo chmod a+rw /dev/ttyUSB1

	sudo chmod a+rw /dev/ttyACM0

	roslaunch jv_moveit_config demo.launch 
	
	roslaunch jarvis robot_slamjoy.launch
	
	roslaunch jarvis gmapping_movebase.launch
	
	roslaunch jarvis startall.launch
	
#### robot_inspection

	roscore

	sudo chmod a+rw /dev/ttyUSB0

	sudo chmod a+rw /dev/ttyUSB1

	sudo chmod a+rw /dev/ttyACM0

	roslaunch jv_moveit_config demo.launch 

	roslaunch jarvis movebase.launch

	roslaunch jarvis start_inspection_rcap2017.launch 

	rosrun rosserial_python serial_node.py _port:=/dev/ttyUSB1 _baud:=57600

	rosrun sound_play soundplay_node.py

	roslaunch openni_launch openni.launch

	rosrun jarvis door.py

#### sound_localization

	roscore

	sudo chmod a+rw /dev/ttyUSB0

	sudo chmod a+rw /dev/ttyUSB1

	sudo chmod a+rw /dev/ttyACM0

	roslaunch jv_moveit_config demo.launch

	roslaunch jarvis movebase.launch

	roslaunch jarvis start_soundlocalization.launch 

	rosrun rosserial_python serial_node.py _port:=/dev/ttyUSB1 _baud:=57600

	rosrun sound_play soundplay_node.py

	roslaunch jarvis xy_to_xyz.launch 

	roslaunch hark_sound_source_localization pr2_kinect.launch

	rosrun jarvis save_image.py 

	roscd speech_jv/

	cd scripts/

	python speech_spr.py

	cat /proc/asound/cards

#### helpmecarry

	roscore

	sudo chmod a+rw /dev/ttyUSB0

	sudo chmod a+rw /dev/ttyUSB1

	sudo chmod a+rw /dev/ttyACM0

	roslaunch jv_moveit_config demo.launch

	roslaunch jarvis followme.launch 

	roslaunch jarvis xy_to_xyz.launch 

	rosrun rosserial_python serial_node.py _port:=/dev/ttyUSB1 _baud:=57600

	rosrun sound_play soundplay_node.py

	roslaunch darknet_ros darknet_ros.launch

	roslaunch jarvis start_helpmecarry.launch 
	
	roscd speech_jv/

	cd scripts/

	python speech_hmc.py

#### open_challenge

	roscore

	sudo chmod a+rw /dev/ttyUSB0

	sudo chmod a+rw /dev/ttyUSB1

	sudo chmod a+rw /dev/ttyACM0

	roslaunch jv_moveit_config demo.launch

	roslaunch jarvis movebase.launch

	roslaunch jarvis xy_to_xyz.launch 

	roslaunch jarvis start_open_challenge.launch

	rosrun sound_play soundplay_node.py

	python speech_op.py 


#!/usr/bin/env python
# import roslib
# roslib.load_manifest('xy_to_xyz')
import sys
import rospy
import cv2
from std_msgs.msg import Bool
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

import numpy as np

class image_converter:


	def __init__(self):

		self.bridge = CvBridge()
		self.sub_image_darknet = rospy.Subscriber('/darknet_ros/detection_image',Image,self.callback_image)
		self.sub_image_save = rospy.Subscriber('/sub_from_main_save',Bool,self.callback_save)
		self.n=0;
		print "init finish"

	def callback_image(self,data):
		try:
			self.cv_save = self.bridge.imgmsg_to_cv2(data, "bgr8")
		except CvBridgeError as e:
			print(e)
	def callback_save(self,data):
		if data.data:
			cv2.imwrite('/media/lossofme/UUI/people_detection('+str(self.n)+').jpg', self.cv_save)
			cv2.imwrite('people_detection('+str(self.n)+').jpg', self.cv_save)
			print "save"
			self.n=self.n+1
		else :
			self.n=0
			print "clear"

def main(args):
	ic = image_converter()
	rospy.init_node('door_node', anonymous=True)
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
	cv2.destroyAllWindows()

if __name__ == '__main__':
		main(sys.argv)

